# TIMERAPP

## Motivation

Every JavaScript framework is delightful when it comes to TodoList. But what happens when there is more moving parts? TimerApp is sandbox for testing React related frameworks and tools.

## Installing
1. git clone https://rpukitis@bitbucket.org/rpukitis/timerapp.git
1. npm i
1. npm start
1. http://localhost:3000/

![alt text](./readme.gif "App animation")