describe('Project', () => {
  it('should create new project', () => {
    cy.visit('http://localhost:3000')
    cy.get('[data-test="toProject"]>div')
      .click()
    cy.get('[data-test="input"]')
      .type('Awesome project 004')
    cy.get('[data-test="clientSelect"]>a')
      .click()
    cy.get('[data-test="clientItem"]')
      .first()
      .click()
    cy.get('[data-test="colorSelect"]')
      .click()
    cy.get('[data-test="colorItem"]')
      .last()
      .click()
    cy.get('[data-test="createButton"]')
      .last()
      .click()
  })

  it('should open and close project edit modal', () => {
    cy.visit('http://localhost:3000')
    cy.get('[data-test="toProject"]>div')
      .click()
    cy.get('[data-test="openEditModal"]')
      .first()
      .click()
    cy.get('[data-test="modalClose"]')
      .click()
  })

  it('should open modal and edit project', () => {
    cy.visit('http://localhost:3000')
    cy.get('[data-test="toProject"]>div')
      .click()
    cy.get('[data-test="openEditModal"]')
      .first()
      .click()
    cy.get('[data-test="modal"]')
      .find('[data-test="input"]')
      .clear()
      .type('Awesome project 004')
    cy.get('[data-test="modal"]')
      .find('[data-test="clientSelect"]>a')
      .click()
    cy.get('[data-test="modal"]')
      .find('[data-test="clientItem"]')
      .last()
      .click()
    cy.get('[data-test="modal"]')
      .find('[data-test="colorSelect"]')
      .click()
    cy.get('[data-test="modal"]')
      .find('[data-test="colorItem"]')
      .last()
      .click()
    cy.get('[data-test="editUpdate"]')
      .click()
  })

  it('should open modal and delete project', () => {
    cy.visit('http://localhost:3000')
    cy.get('[data-test="toProject"]>div')
      .click()
    cy.get('[data-test="openEditModal"]')
      .first()
      .click()
    cy.get('[data-test="editDelete"]')
      .click()
  })
})
