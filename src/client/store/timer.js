import { addEntry } from './entries'
import { getElapsedTime, getElapsedTimeHM, getMillisecondsFromHM } from '../services'

export const START_TIMER = 'START_TIMER'
export const STOP_TIMER = 'STOP_TIMER'
export const RESET_TIMER = 'RESET_TIMER'
export const UPDATE_TIMER_DESCRIPTION = 'UPDATE_TIMER_DESCRIPTION'
export const UPDATE_TIMER_PROJECT = 'UPDATE_TIMER_PROJECT'
export const UPDATE_TIMER_TAGS = 'UPDATE_TIMER_TAGS'
export const SET_DATE_TIME = 'SET_DATE_TIME'
export const RESET_DATE_TIME = 'RESET_DATE_TIME'

export const startTimer = now => ({
  type: START_TIMER,
  now,
})

export const stopTimer = now => ({
  type: STOP_TIMER,
  now,
})

export const resetTimer = () => ({
  type: RESET_TIMER,
})

export const updateTimerDescription = description => ({
  type: UPDATE_TIMER_DESCRIPTION,
  description,
})

export const updateTimerProject = project => ({
  type: UPDATE_TIMER_PROJECT,
  project,
})

export const updateTimerTags = tags => ({
  type: UPDATE_TIMER_TAGS,
  tags,
})

export const setTimerDateTime = (startedAt, stoppedAt, date) => ({
  type: SET_DATE_TIME,
  startedAt,
  stoppedAt,
  date,
})

export const resetTimerDateTime = () => ({
  type: RESET_DATE_TIME,
})

export const startTimerFromEntry = (project, description, tags, now = new Date().getTime()) =>
  (dispatch) => {
    dispatch(updateTimerProject(project))
    dispatch(updateTimerDescription(description))
    dispatch(updateTimerTags(tags))
    dispatch(startTimer(now))
  }

export const addEntryFromTimer = now => (dispatch, getState) => {
  dispatch(stopTimer(now))

  const { description, project, tags, startedAt, stoppedAt } = getState().timer
  const duration = getElapsedTime(startedAt, stoppedAt)

  dispatch(resetTimer())
  dispatch(addEntry(description, project, tags, duration, startedAt))
}

export const addEntryFromDateTime = () => (dispatch, getState) => {
  const { description, project, tags, startedAt, stoppedAt, date } = getState().timer
  const started = getMillisecondsFromHM(startedAt)
  const duration = getElapsedTimeHM(startedAt, stoppedAt)

  dispatch(resetTimer())
  dispatch(addEntry(description, project, tags, duration, date + started))
}

export default (state = {}, action) => {
  switch (action.type) {
    case START_TIMER:
      return {
        ...state,
        startedAt: action.now,
        stoppedAt: undefined,
      }
    case STOP_TIMER:
      return {
        ...state,
        stoppedAt: action.now,
      }
    case RESET_TIMER:
      return {
        startedAt: undefined,
        stoppedAt: undefined,
      }
    case UPDATE_TIMER_DESCRIPTION:
      return {
        ...state,
        description: action.description,
      }
    case UPDATE_TIMER_PROJECT:
      return {
        ...state,
        project: action.project,
      }
    case UPDATE_TIMER_TAGS:
      return {
        ...state,
        tags: action.tags,
      }
    case SET_DATE_TIME:
      return {
        ...state,
        startedAt: action.startedAt,
        stoppedAt: action.stoppedAt,
        date: action.date,
      }
    case RESET_DATE_TIME:
      return {
        ...state,
        startedAt: undefined,
        stoppedAt: undefined,
        date: undefined,
      }
    default:
      return state
  }
}
