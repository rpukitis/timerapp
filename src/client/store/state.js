const state = {
  timer: {
    startedAt: undefined,
    stoppedAt: undefined,
  },
  entries: {
    [-100]: {
      id: -100,
      description: 'developing awesome app',
      project: -100,
      duration: 2700000,
      date: new Date().getTime() - 2700000,
    },
    [-101]: {
      id: -101,
      description: 'developing awesome app',
      project: -100,
      duration: 2700000,
      date: new Date().getTime() - (1000 * 60 * 60 * 24),
    },
    [-102]: {
      id: -102,
      description: 'developing awesome app',
      project: -100,
      duration: 2700000,
      date: new Date().getTime() - (1000 * 60 * 60 * 48),
    },
    [-103]: {
      id: -103,
      description: 'developing awesome app',
      project: -100,
      duration: 2700000,
      date: new Date().getTime() - (1000 * 60 * 60 * 24 * 370),
    },
  },
  projects: {
    [-100]: {
      id: -100,
      name: 'Awesome Project 001',
      color: '#06aaf5',
      client: -100,
      status: 80,
    },
    [-101]: {
      id: -101,
      name: 'Awesome Project 002',
      color: '#c56bff',
      client: -101,
      status: 40,
    },
    [-102]: {
      id: -102,
      name: 'Awesome Project 003',
      color: '#e20505',
      client: -101,
      status: 60,
    },
  },
  clients: {
    [-100]: {
      id: -100,
      name: 'awesome client 001',
    },
    [-101]: {
      id: -101,
      name: 'awesome client 002',
    },
  },
  tags: {
    [-100]: {
      id: -100,
      name: 'front-end',
    },
    [-101]: {
      id: -101,
      name: 'html',
    },
    [-102]: {
      id: -102,
      name: 'css',
    },
    [-103]: {
      id: -103,
      name: 'JavaScript',
    },
    [-104]: {
      id: -104,
      name: 'react',
    },
    [-105]: {
      id: -105,
      name: 'redux',
    },
    [-106]: {
      id: -106,
      name: 'back-end',
    },
    [-107]: {
      id: -107,
      name: 'node',
    },
    [-108]: {
      id: -108,
      name: 'express',
    },
    [-109]: {
      id: -109,
      name: 'sql',
    },
  },
}

export default state

export const dates = [
  new Date(2017, 11, 17).getTime() - 2700000,
  new Date(2017, 11, 17).getTime() - (1000 * 60 * 60 * 24),
  new Date(2017, 11, 17).getTime() - (1000 * 60 * 60 * 48),
  new Date(2017, 11, 17).getTime() - (1000 * 60 * 60 * 24 * 370),
  new Date(2017, 11, 17).getTime() - (1000 * 60 * 60 * 24 * 370),
]
export const test = {
  ...state,
  timer: {
    project: -100,
  },
  entries: Object
    .values(state.entries)
    .reduce((acc, e, i) => {
      acc[e.id] = {
        ...e,
        date: dates[i],
      }
      return acc
    }, {}),
}
