import { deleteMapIdValue } from '../services'

export const ADD_TAG = 'ADD_TAG'
export const UPDATE_TAG = 'UPDATE_TAG'
export const REMOVE_TAG = 'REMOVE_TAG'

export let addNextTagID = 1
export const addTag = name => ({
  type: ADD_TAG,
  id: addNextTagID++, // eslint-disable-line
  name,
})

export const updateTag = (id, name) => ({
  type: UPDATE_TAG,
  id,
  name,
})

export const removeTag = id => ({
  type: REMOVE_TAG,
  id,
})

const tag = (state, action) => {
  switch (action.type) {
    case ADD_TAG:
      return {
        id: action.id,
        name: action.name,
      }
    case UPDATE_TAG:
      return {
        ...state,
        name: action.name,
      }
    default:
      return state
  }
}

export default (state = [], action) => {
  switch (action.type) {
    case ADD_TAG:
      return {
        ...state,
        [action.id]: tag(undefined, action),
      }
    case UPDATE_TAG:
      return {
        ...state,
        [action.id]: tag(state[action.id], action),
      }
    case REMOVE_TAG:
      return { ...deleteMapIdValue(action.id, state) }
    default:
      return state
  }
}
