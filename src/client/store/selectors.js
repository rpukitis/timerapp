import { createSelector } from 'reselect'
import { groupByClient, groupByDate, sortByDate } from '../services'

export const getTimer = (state) => state.timer
export const getEntries = (state) => state.entries
export const getEntryArray = createSelector(
  getEntries,
  entries => Object.values(entries),
)
export const getProjects = (state) => state.projects
export const getProjectArray = createSelector(
  getProjects,
  projects => Object.values(projects),
)
export const getClients = (state) => state.clients
export const getClientArray = createSelector(
  getClients,
  clients => Object.values(clients),
)
export const getTags = (state) => state.tags
export const getTagArray = createSelector(
  getTags,
  tags => Object.values(tags),
)
export const getTimerProject = createSelector(
  getTimer,
  getProjects,
  (timer, projects) => projects[timer.project],
)
export const getProjectsGroupedByClient = createSelector(
  getProjectArray,
  getClientArray,
  (projects, clients) => groupByClient(projects, clients),
)

export const getEntriesGroupedByDate = createSelector(
  getEntryArray,
  (entries) => groupByDate(sortByDate(entries)),
)

export const getEntryGroup = createSelector(
  getEntriesGroupedByDate,
  (entries) => Array.from(entries.keys()),
)
