import { deleteMapIdValue } from '../services'

export const ADD_PROJECT = 'ADD_PROJECT'
export const UPDATE_PROJECT = 'UPDATE_PROJECT'
export const REMOVE_PROJECT = 'REMOVE_PROJECT'

export let addProjectNextID = 1
export const addProject = (name, color, client, status) => ({
  type: ADD_PROJECT,
  id: addProjectNextID++, // eslint-disable-line
  name,
  color,
  client,
  status,
})

export const updateProject = (id, name, color, client) => ({
  type: UPDATE_PROJECT,
  id,
  name,
  color,
  client,
})

export const removeProject = id => ({
  type: REMOVE_PROJECT,
  id,
})

const project = (state, action) => {
  switch (action.type) {
    case ADD_PROJECT:
      return {
        id: action.id,
        name: action.name,
        color: action.color,
        client: action.client,
        status: action.status,
      }
    case UPDATE_PROJECT:
      return {
        ...state,
        name: action.name,
        color: action.color,
        client: action.client,
      }
    default:
      return state
  }
}

export default (state = {}, action) => {
  switch (action.type) {
    case ADD_PROJECT:
      return {
        ...state,
        [action.id]: project(undefined, action),
      }
    case UPDATE_PROJECT:
      return {
        ...state,
        [action.id]: project(state[action.id], action),
      }
    case REMOVE_PROJECT:
      return { ...deleteMapIdValue(action.id, state) }
    default:
      return state
  }
}
