import reducer, * as tags from './tags'

describe('tags actions', () => {
  it('should create new tag', () => {
    const name = 'awesome tag'
    const expected = {
      type: tags.ADD_TAG,
      id: tags.addNextTagID,
      name,
    }
    expect(tags.addTag(name)).toEqual(expected)
  })

  it('should update existing tag', () => {
    const id = 101
    const name = 'awesome tag'
    const expected = {
      type: tags.UPDATE_TAG,
      id,
      name,
    }
    expect(tags.updateTag(id, name)).toEqual(expected)
  })

  it('should remove existing tag', () => {
    const id = 101
    const expected = {
      type: tags.REMOVE_TAG,
      id,
    }
    expect(tags.removeTag(id)).toEqual(expected)
  })
})

describe('tags reducers', () => {
  it('should handle ADD_TAG', () => {
    const id = 101
    const name = 'awesome name'
    expect(
      reducer({}, {
        type: tags.ADD_TAG,
        id,
        name,
      })
    ).toEqual({
      [id]: {
        id,
        name,
      },
    })
  })

  it('should handle UPDATE_TAG', () => {
    const id = 101
    const name = 'awesome name'
    expect(
      reducer({
        [id]: {
          name: 'some name before',
          id,
        },
      }, {
        type: tags.UPDATE_TAG,
        id,
        name,
      })
    ).toEqual({
      [id]: {
        id,
        name,
      },
    })
  })

  it('should handle REMOVE_TAG', () => {
    const id = 101
    const name = 'awesome name'
    expect(
      reducer({
        [id]: {
          id,
          name,
        },
      }, {
        type: tags.REMOVE_TAG,
        id,
      })
    ).toEqual({ [id]: undefined })
  })
})
