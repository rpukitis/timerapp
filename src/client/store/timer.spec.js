import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import reducer, * as timer from './timer'
import { ADD_ENTRY, addEntryNextID } from './entries'

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)

describe('timer actions', () => {
  it('should create an action to start timer', () => {
    const now = new Date().getTime()
    const expected = {
      type: timer.START_TIMER,
      now,
    }
    expect(timer.startTimer(now)).toEqual(expected)
  })

  it('should create an action to stop timer', () => {
    const now = new Date().getTime()
    const expected = {
      type: timer.STOP_TIMER,
      now,
    }
    expect(timer.stopTimer(now)).toEqual(expected)
  })

  it('should create an action to reset timer', () => {
    const expected = {
      type: timer.RESET_TIMER,
    }
    expect(timer.resetTimer()).toEqual(expected)
  })

  it('should create an action to update timer description', () => {
    const description = 'awesome description'
    const expected = {
      type: timer.UPDATE_TIMER_DESCRIPTION,
      description,
    }
    expect(timer.updateTimerDescription(description)).toEqual(expected)
  })

  it('should create an action to update timer project', () => {
    const project = 101
    const expected = {
      type: timer.UPDATE_TIMER_PROJECT,
      project,
    }
    expect(timer.updateTimerProject(project)).toEqual(expected)
  })

  it('should create an action to update timer tags', () => {
    const tags = [101, 102, 103, 104, 105]
    const expected = {
      type: timer.UPDATE_TIMER_TAGS,
      tags,
    }
    expect(timer.updateTimerTags(tags)).toEqual(expected)
  })

  it('should create an action to set timer date and time', () => {
    const now = new Date()
    const stoppedAt = now.getTime()
    const startedAt = stoppedAt - 1000 * 60 * 45
    const date = new Date(now.getFullYear(), now.getMonth(), now.getDate()).getTime()
    const expected = {
      type: timer.SET_DATE_TIME,
      startedAt,
      stoppedAt,
      date,
    }
    expect(timer.setTimerDateTime(startedAt, stoppedAt, date)).toEqual(expected)
  })

  it('should create an action to reset timer date and time', () => {
    const expected = {
      type: timer.RESET_DATE_TIME,
    }
    expect(timer.resetTimerDateTime()).toEqual(expected)
  })

  it('should start timer from entry list items', () => {
    const now = new Date().getTime()
    const description = 'awesome description'
    const project = 101
    const tags = [101, 102, 103, 104, 105]
    const expected = [
      { type: timer.UPDATE_TIMER_PROJECT, project },
      { type: timer.UPDATE_TIMER_DESCRIPTION, description },
      { type: timer.UPDATE_TIMER_TAGS, tags },
      { type: timer.START_TIMER, now },
    ]
    const store = mockStore()
    store.dispatch(timer.startTimerFromEntry(project, description, tags, now))
    expect(store.getActions()).toEqual(expected)
  })

  it('should add new entry from timer to list', () => {
    const now = new Date().getTime()
    const startedAt = now - 1000
    const stoppedAt = now
    const description = 'awesome description'
    const project = 101
    const tags = [101, 102, 103, 104, 105]
    const expected = [
      { type: timer.STOP_TIMER, now },
      { type: timer.RESET_TIMER },
      { type: ADD_ENTRY,
        id: addEntryNextID,
        date: startedAt,
        duration: 1000,
        description,
        project,
        tags,
      },
    ]
    const store = mockStore({
      timer: {
        description,
        project,
        tags,
        startedAt,
        stoppedAt,
      },
    })
    store.dispatch(timer.addEntryFromTimer(now))
    expect(store.getActions()).toEqual(expected)
  })

  it('should add new entry from timer date/time calendar to list', () => {
    const startedAt = '09:00'
    const stoppedAt = '09:45'
    const description = 'awesome description'
    const project = 101
    const tags = [101, 102, 103, 104, 105]
    const date = new Date(2017, 11, 17).getTime()
    const expected = [
      { type: timer.RESET_TIMER },
      { type: ADD_ENTRY,
        id: addEntryNextID,
        duration: 1000 * 60 * 45,
        date: date + 1000 * 60 * 60 * 9,
        description,
        project,
        tags,
      },
    ]
    const store = mockStore({
      timer: {
        description,
        project,
        tags,
        startedAt,
        stoppedAt,
        date,
      },
    })
    store.dispatch(timer.addEntryFromDateTime())
    expect(store.getActions()).toEqual(expected)
  })
})

describe('timer reducers', () => {
  it('should handle START_TIMER', () => {
    const now = new Date().getTime()
    expect(
      reducer({}, {
        type: timer.START_TIMER,
        now,
      })
    ).toEqual({
      startedAt: now,
      stoppedAt: undefined,
    })
  })

  it('should handle STOP_TIMER', () => {
    const now = new Date().getTime()
    expect(
      reducer({}, {
        type: timer.STOP_TIMER,
        now,
      })
    ).toEqual({
      stoppedAt: now,
    })
  })

  it('should handle RESET_TIMER', () => {
    const now = new Date().getTime()
    expect(
      reducer({
        startedAt: now - 1000,
        stoppedAt: now,
      }, {
        type: timer.RESET_TIMER,
      })
    ).toEqual({
      startedAt: undefined,
      stoppedAt: undefined,
    })
  })

  it('should handle UPDATE_TIMER_DESCRIPTION', () => {
    const description = 'awesome description'
    expect(
      reducer({}, {
        type: timer.UPDATE_TIMER_DESCRIPTION,
        description,
      })
    ).toEqual({
      description,
    })
  })

  it('should handle UPDATE_TIMER_PROJECT', () => {
    const project = 101
    expect(
      reducer({}, {
        type: timer.UPDATE_TIMER_PROJECT,
        project,
      })
    ).toEqual({
      project,
    })
  })

  it('should handle UPDATE_TIMER_TAGS', () => {
    const tags = [101, 102, 103, 104, 105]
    expect(
      reducer({}, {
        type: timer.UPDATE_TIMER_TAGS,
        tags,
      })
    ).toEqual({
      tags,
    })
  })

  it('should handle SET_DATE_TIME', () => {
    const startedAt = '09:00'
    const stoppedAt = '09:45'
    const date = new Date(2017, 11, 17).getTime()
    expect(
      reducer({}, {
        type: timer.SET_DATE_TIME,
        startedAt,
        stoppedAt,
        date,
      })
    ).toEqual({
      startedAt,
      stoppedAt,
      date,
    })
  })

  it('should handle RESET_DATE_TIME', () => {
    expect(
      reducer({
        startedAt: '09:00',
        stoppedAt: '09:45',
        date: new Date(2017, 11, 17).getTime(),
      }, {
        type: timer.RESET_DATE_TIME,
      })
    ).toEqual({
      startedAt: undefined,
      stoppedAt: undefined,
      date: undefined,
    })
  })
})
