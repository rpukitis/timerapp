import reducer, * as clients from './clients'

describe('clients actions', () => {
  it('should create new client', () => {
    const name = 'awesome client'
    const expected = {
      type: clients.ADD_CLIENT,
      id: clients.addClientNextID,
      name,
    }
    expect(clients.addClient(name)).toEqual(expected)
  })

  it('should update existing client', () => {
    const id = 101
    const name = 'awesome client'
    const expected = {
      type: clients.UPDATE_CLIENT,
      id,
      name,
    }
    expect(clients.updateClient(id, name)).toEqual(expected)
  })

  it('should remove existing client', () => {
    const id = 101
    const expected = {
      type: clients.REMOVE_CLIENT,
      id,
    }
    expect(clients.removeClient(id)).toEqual(expected)
  })
})

describe('clients reducers', () => {
  it('should handle ADD_CLIENT', () => {
    const id = 101
    const name = 'awesome name'
    expect(
      reducer({}, {
        type: clients.ADD_CLIENT,
        id,
        name,
      })
    ).toEqual({
      [id]: {
        id,
        name,
      },
    })
  })

  it('should handle UPDATE_CLIENT', () => {
    const id = 101
    const name = 'awesome name'
    expect(
      reducer({
        [id]: {
          name: 'some name before',
          id,
        },
      }, {
        type: clients.UPDATE_CLIENT,
        id,
        name,
      })
    ).toEqual({
      [id]: {
        id,
        name,
      },
    })
  })

  it('should handle REMOVE_CLIENT', () => {
    const id = 101
    const name = 'awesome name'
    expect(
      reducer({
        [id]: {
          id,
          name,
        },
      }, {
        type: clients.REMOVE_CLIENT,
        id,
      })
    ).toEqual({ [id]: undefined })
  })
})
