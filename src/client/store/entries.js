import { deleteMapIdValue } from '../services'

export const ADD_ENTRY = 'ADD_ENTRY'
export const REMOVE_ENTRY = 'REMOVE_ENTRY'

export let addEntryNextID = 1
export const addEntry = (description, project, tags, duration, date) => ({
  type: 'ADD_ENTRY',
  id: addEntryNextID++, // eslint-disable-line
  description,
  project,
  tags,
  duration,
  date,
})

export const removeEntry = id => ({
  type: REMOVE_ENTRY,
  id,
})

const entry = (state, action) => {
  switch (action.type) {
    case ADD_ENTRY:
      return {
        id: action.id,
        description: action.description,
        project: action.project,
        tags: action.tags,
        duration: action.duration,
        date: action.date,
      }
    default:
      return state
  }
}

export default (state = {}, action) => {
  switch (action.type) {
    case ADD_ENTRY:
      return {
        ...state,
        [action.id]: entry(undefined, action),
      }
    case REMOVE_ENTRY:
      return { ...deleteMapIdValue(action.id, state) }
    default:
      return state
  }
}
