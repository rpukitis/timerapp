import reducer, * as projects from './projects'

describe('projects actions', () => {
  it('should create new project', () => {
    const name = 'awesome project'
    const color = '#c7741c'
    const client = 101
    const status = 160
    const expected = {
      type: projects.ADD_PROJECT,
      id: projects.addProjectNextID,
      name,
      color,
      client,
      status,
    }
    expect(projects.addProject(name, color, client, status)).toEqual(expected)
  })

  it('should update existing project', () => {
    const id = 101
    const name = 'awesome project'
    const color = '#c7741c'
    const client = 101
    const expected = {
      type: projects.UPDATE_PROJECT,
      id,
      name,
      color,
      client,
    }
    expect(projects.updateProject(id, name, color, client)).toEqual(expected)
  })

  it('should remove existing project', () => {
    const id = 101
    const expected = {
      type: projects.REMOVE_PROJECT,
      id,
    }
    expect(projects.removeProject(id)).toEqual(expected)
  })
})

describe('projects reducers', () => {
  it('should handle ADD_PROJECT', () => {
    const id = 101
    const name = 'awesome project'
    const color = '#c7741c'
    const client = 101
    const status = 160
    expect(
      reducer({}, {
        type: projects.ADD_PROJECT,
        id,
        name,
        color,
        client,
        status,
      })
    ).toEqual({
      [id]: {
        id,
        name,
        color,
        client,
        status,
      },
    })
  })

  it('should handle UPDATE_PROJECT', () => {
    const id = 101
    const name = 'awesome project'
    const color = '#c7741c'
    const client = 101
    expect(
      reducer({
        [id]: {
          name: 'before update',
          color: '#3750b5',
          client: 105,
          id,
        },
      }, {
        type: projects.UPDATE_PROJECT,
        id,
        name,
        color,
        client,
      })
    ).toEqual({
      [id]: {
        id,
        name,
        color,
        client,
      },
    })
  })

  it('should handle REMOVE_PROJECT', () => {
    const id = 101
    expect(
      reducer({
        [id]: {
          name: 'awesome projects',
          color: '#c7741c',
          client: 105,
          status: 160,
          id,
        },
      }, {
        type: projects.REMOVE_PROJECT,
        id,
      })
    ).toEqual({[id]: undefined})
  })
})
