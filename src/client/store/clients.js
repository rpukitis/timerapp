import { deleteMapIdValue } from '../services'

export const ADD_CLIENT = 'ADD_CLIENT'
export const UPDATE_CLIENT = 'UPDATE_CLIENT'
export const REMOVE_CLIENT = 'REMOVE_CLIENT'

export let addClientNextID = 1
export const addClient = name => ({
  type: ADD_CLIENT,
  id: addClientNextID++, // eslint-disable-line
  name,
})

export const updateClient = (id, name) => ({
  type: UPDATE_CLIENT,
  id,
  name,
})

export const removeClient = id => ({
  type: REMOVE_CLIENT,
  id,
})

const client = (state, action) => {
  switch (action.type) {
    case ADD_CLIENT:
      return {
        id: action.id,
        name: action.name,
      }
    case UPDATE_CLIENT:
      return {
        ...state,
        name: action.name,
      }
    default:
      return state
  }
}

export default (state = {}, action) => {
  switch (action.type) {
    case ADD_CLIENT:
      return {
        ...state,
        [action.id]: client(undefined, action),
      }
    case UPDATE_CLIENT:
      return {
        ...state,
        [action.id]: client(state[action.id], action),
      }
    case REMOVE_CLIENT:
      return { ...deleteMapIdValue(action.id, state) }
    default:
      return state
  }
}
