import reducer, * as entries from './entries'

describe('entries actions', () => {
  it('should create new entry', () => {
    const description = 'awesome work'
    const project = 101
    const tags = [101, 102, 103, 104, 105]
    const duration = 1000 * 60 * 45
    const date = new Date().getTime()
    const expected = {
      type: entries.ADD_ENTRY,
      id: entries.addEntryNextID,
      description,
      project,
      tags,
      duration,
      date,
    }
    expect(entries.addEntry(description, project, tags, duration, date)).toEqual(expected)
  })

  it('should remove entry form list', () => {
    const id = 101
    const expected = {
      type: entries.REMOVE_ENTRY,
      id,
    }
    expect(entries.removeEntry(id)).toEqual(expected)
  })
})

describe('entries reduces', () => {
  it('should handle ADD_ENTRY', () => {
    const id = 101
    const description = 'awesome work'
    const project = 101
    const tags = [101, 102, 103, 104, 105]
    const duration = 1000 * 60 * 45
    const date = new Date().getTime()
    expect(
      reducer({}, {
        type: entries.ADD_ENTRY,
        id,
        description,
        project,
        tags,
        duration,
        date,
      })
    ).toEqual({
      [id]: {
        id,
        description,
        project,
        tags,
        duration,
        date,
      },
    })
  })

  it('should handle REMOVE_ENTRY', () => {
    const id = 101
    expect(
      reducer({
        [id]: {
          description: 'awesome work',
          project: 101,
          tags: [101, 102, 103, 104, 105],
          duration: 1000 * 60 * 45,
          date: new Date().getTime(),
          id,
        },
      }, {
        type: entries.REMOVE_ENTRY,
        id,
      })
    ).toEqual({[id]: undefined})
  })
})
