import { combineReducers } from 'redux'
import timer from './timer'
import entries from './entries'
import projects from './projects'
import clients from './clients'
import tags from './tags'

const app = combineReducers({
  timer,
  entries,
  projects,
  clients,
  tags,
})

export default app
