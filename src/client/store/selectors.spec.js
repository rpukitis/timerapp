import * as selectors from './selectors'
import { test as state, dates } from './state'

describe('selectors', () => {
  it('should return timer state', () => {
    expect(selectors.getTimer(state)).toEqual(state.timer)
  })

  it('should return entries state', () => {
    expect(selectors.getEntries(state)).toEqual(state.entries)
  })

  it('should return entries as array', () => {
    expect(selectors.getEntryArray(state)).toEqual(Object.values(state.entries))
  })

  it('should return projects state', () => {
    expect(selectors.getProjects(state)).toEqual(state.projects)
  })

  it('should return projects as array', () => {
    expect(selectors.getProjectArray(state)).toEqual(Object.values(state.projects))
  })

  it('should return clients state', () => {
    expect(selectors.getClients(state)).toEqual(state.clients)
  })

  it('should return clients as array', () => {
    expect(selectors.getClientArray(state)).toEqual(Object.values(state.clients))
  })

  it('should return tags state', () => {
    expect(selectors.getTags(state)).toEqual(state.tags)
  })

  it('should return tags as array', () => {
    expect(selectors.getTagArray(state)).toEqual(Object.values(state.tags))
  })

  it('should return timer project', () => {
    expect(selectors.getTimerProject(state)).toEqual(state.projects[state.timer.project])
  })

  it('should return projects grouped by client', () => {
    expect(selectors.getProjectsGroupedByClient(state)).toEqual(
      [
        {
          client: 'awesome client 001',
          projects: [
            {
              client: -100,
              color: '#06aaf5',
              id: -100,
              name: 'Awesome Project 001',
              status: 80,
            },
          ],
        },
        {
          client: 'awesome client 002',
          projects: [
            {
              client: -101,
              color: '#c56bff',
              id: -101,
              name: 'Awesome Project 002',
              status: 40,
            },
            {
              client: -101,
              color: '#e20505',
              id: -102,
              name: 'Awesome Project 003',
              status: 60,
            },
          ],
        },
      ]
    )
  })

  it('should return entries grouped by date', () => {
    expect(selectors.getEntriesGroupedByDate(state)).toEqual(
      new Map(
        [
          ['16/12/17', [
            {
              date: dates[0],
              description: 'developing awesome app',
              duration: 2700000,
              id: -100,
              project: -100,
            },
            {
              date: dates[1],
              description: 'developing awesome app',
              duration: 2700000,
              id: -101,
              project: -100,
            },
          ]],
          ['15/12/17', [
            {
              date: dates[2],
              description: 'developing awesome app',
              duration: 2700000,
              id: -102,
              project: -100,
            },
          ]],
          ['12/12/16', [
            {
              date: dates[3],
              description: 'developing awesome app',
              duration: 2700000,
              id: -103,
              project: -100,
            },
          ]],
        ]
      )
    )
  })

  it('should return entry groups', () => {
    expect(selectors.getEntryGroup(state)).toEqual(['16/12/17', '15/12/17', '12/12/16'])
  })
})
