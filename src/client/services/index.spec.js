import * as module from './index'

describe('services', () => {
  it('should pad hours and minutes to HH:MM form', () => {
    expect(module.getTimeStamp(7, 7)).toBe('07:07')
    expect(module.getTimeStamp(12, 12)).toBe('12:12')
  })

  it('should validate time and if necessary add colon or return default time', () => {
    expect(module.validateTimeStamp('25:56', '00:00')).toBe('00:00')
    expect(module.validateTimeStamp('17:61', '00:00')).toBe('00:00')
    expect(module.validateTimeStamp('1734', '00:00')).toBe('17:34')
    expect(module.validateTimeStamp('17:34', '00:00')).toBe('17:34')
  })

  it('should compute elapsed time from started and stopped time', () => {
    const minutes45 = 1000 * 60 * 45
    const startedAt = 1487959479098
    const stoppedAt = startedAt + minutes45
    expect(module.getElapsedTime(undefined, stoppedAt)).toBe(0)
    expect(module.getElapsedTime(startedAt, stoppedAt)).toBe(minutes45)
  })

  it('should compute milliseconds from HH:MM form', () => {
    const time = '14:15'
    const hh = 1000 * 60 * 60 * 14
    const mm = 1000 * 60 * 15
    const milliseconds = hh + mm
    expect(module.getMillisecondsFromHM(time)).toBe(milliseconds)
  })

  it('should compute elapsed milliseconds from started and stopped time', () => {
    expect(module.getElapsedTimeHM('11:54', '10:54')).toBe(82800000)
    expect(module.getElapsedTimeHM('10:54', '11:54')).toBe(3600000)
    expect(module.getElapsedTimeHM('10:54', '14:38')).toBe(13440000)
    expect(module.getElapsedTimeHM('10:54', '23:15')).toBe(44460000)
  })

  it('should transform time to 00:00:00 form', () => {
    const seconds45 = 1000 * 45
    const minutes45 = 1000 * 60 * 45
    const hours24 = 1000 * 60 * 60 * 24
    const elapsed = hours24 + minutes45 + seconds45
    expect(module.getDurationStamp(undefined)).toBe('00:00:00')
    expect(module.getDurationStamp(elapsed)).toBe('24:45:45')
  })

  it('should convert milliseconds to HH:MM form', () => {
    const milliseconds = new Date().getTime()
    const evaluation = module.millisecondsToHM(milliseconds)
    expect(evaluation).toMatch(/^([01]\d|2[0-3]):([0-5]\d)$/)
  })

  it('should compute elapsed "HH:MM-HH:MM" from date and duration', () => {
    const milliseconds = new Date().getTime()
    const duration = 1000 * 60 * 14 * 1
    const evaluation = module.getFromToStamp(milliseconds, duration).split('-')
    expect(evaluation).toHaveLength(2)
    expect(evaluation[0]).toMatch(/^([01]\d|2[0-3]):([0-5]\d)$/)
    expect(evaluation[1]).toMatch(/^([01]\d|2[0-3]):([0-5]\d)$/)
  })

  it('should sort time entries by their date property', () => {
    const now = new Date().getTime()
    const entries = [{ date: now }, { date: now + 1000 }]
    const sorted = [{ date: now + 1000 }, { date: now }]
    expect(module.sortByDate(entries)).toEqual(sorted)
  })

  it('should group time entries by date', () => {
    const entries = [
      {
        id: 1,
        group: '24/02/17',
        date: 1487959479098,
      },
      {
        id: 2,
        group: '23/02/17',
        date: 1487959479098 - 1000 * 60 * 60 * 24,
      },
      {
        id: 3,
        group: '23/02/17',
        date: 1487959479098 - 1000 * 60 * 60 * 24,
      },
    ]
    const groupedEntries = module.groupByDate(entries)
    const group24 = groupedEntries.get('24/02/17')
    const group23 = groupedEntries.get('23/02/17')
    expect(group24[0].group).toBe('24/02/17')
    expect(group24[0].id).toBe(1)
    expect(group23[0].group).toBe('23/02/17')
    expect(group23[0].id).toBe(2)
    expect(group23[1].group).toBe('23/02/17')
    expect(group23[1].id).toBe(3)
  })

  it('should search array by their object names and given key', () => {
    const a = { name: 'foo' }
    const b = { name: 'fooo' }
    const c = { name: 'bar' }
    const d = { name: 'barzz' }
    const list = [a, b, c, d]
    expect(module.filterByKey('', list)).toEqual([a, b, c, d])
    expect(module.filterByKey('foo', list)).toEqual([a, b])
    expect(module.filterByKey('barz', list)).toEqual([d])
  })

  it('should toggle element in array, remove when array have it and add when not', () => {
    const toggleOn = [1, 2, 3, 4]
    const toggleOff = [2, 3, 4]
    expect(module.toggleElement(1, toggleOn)).toEqual(toggleOff)
    expect(module.toggleElement(1, toggleOff)).toEqual(toggleOn)
  })

  it('should find element name in array by given id or return empty string', () => {
    const list = [{ id: 1, name: 'foo' }, { id: 2, name: 'bar' }]
    expect(module.getNameByID(3, list)).toEqual('')
    expect(module.getNameByID(2, list)).toEqual('bar')
  })

  it('should group projects by their client', () => {
    const projects = [ { client: null }, { client: 1 }, { client: 2 } ]
    const clients = [ { id: 1, name: 'foo' }, { id: 2, name: 'bar' } ]
    const groupedWithout = { client: 'No client', projects: [{ client: null }] }
    const groupedFoo = { client: 'foo', projects: [{ client: 1 }] }
    const groupedBar = { client: 'bar', projects: [{ client: 2 }] }
    expect(module.groupByClient(projects, clients)).toEqual([groupedFoo, groupedBar, groupedWithout])
  })

  it('should search client names in grouped projects', () => {
    const groupedWithout = { client: 'No client', projects: [{ name: 'foo', client: null }] }
    const groupedFoo = { client: 'foo', projects: [{ name: 'foo', client: 1 }] }
    const groupedBar = { client: 'bar', projects: [{ name: 'bar', client: 2 }, { name: 'abc', client: 1 }] }
    const group = [groupedFoo, groupedBar, groupedWithout]
    const result = [{ client: 'bar', projects: [{ name: 'bar', client: 2 }] }]
    expect(module.filterClientGroupByKey('ba', group)).toEqual(result)
  })

  it('should wrap number between min and max', () => {
    expect(module.wrap(0, 0, 2)).toEqual(0)
    expect(module.wrap(1, 0, 2)).toEqual(1)
    expect(module.wrap(2, 0, 2)).toEqual(2)
    expect(module.wrap(3, 0, 2)).toEqual(0)
  })

  it('should compute index from given row column and grid size', () => {
    expect(module.ijToIndex(0, 0, 2)).toEqual(0)
    expect(module.ijToIndex(0, 1, 2)).toEqual(1)
    expect(module.ijToIndex(1, 0, 2)).toEqual(2)
    expect(module.ijToIndex(1, 1, 2)).toEqual(3)
  })

  it('should delete map id value', () => {
    const map = {
      1: { id: 1 },
      2: { id: 2 },
      3: { id: 3 },
    }
    const result = {
      1: { id: 1 },
      3: { id: 3 },
    }
    expect(module.deleteMapIdValue(2, map)).toEqual(result)
  })

  it('should split date from string or get it from date value', () => {
    expect(module.getSelectedDate('Sun Dec 17 2017 12:00:00 GMT+0200 (EET)')).toEqual(17)
    expect(module.getSelectedDate('Sun Dec 18 2017 12:00:00 GMT+0200 (EET)')).toEqual(18)
    expect(module.getSelectedDate(new Date(2017, 11, 17), 2017, 11)).toEqual(17)
    expect(module.getSelectedDate(new Date(2017, 11, 18), 2017, 11)).toEqual(18)
    expect(module.getSelectedDate(new Date(2017, 11, 18), 2017, 10)).toEqual(-1)
    expect(module.getSelectedDate(new Date(2017, 11, 18), 2018, 11)).toEqual(-1)
  })
})
