export const getTimeStamp = (hours, minutes) => (
  String(hours).padStart(2, '0') + ':' +
    String(minutes).padStart(2, '0')
)

export const validateTimeStamp = (time, defaultTime) => {
  if (!time || !time.match(/^([01]\d|2[0-3]):?([0-5]\d)$/)) {
    return defaultTime
  } else if (time.length < 5) {
    const chars = time.split('')
    chars.splice(2, 0, ':')
    return chars.join('')
  }
  return time
}

export const getElapsedTime = (startedAt, stoppedAt) => {
  if (!startedAt) {
    return 0
  }

  return stoppedAt - startedAt
}

export const getMillisecondsFromHM = (time) => {
  const hh = parseInt(time.slice(0, 2), 10)
  const mm = parseInt(time.slice(3), 10)
  return 1000 * 60 * 60 * hh + 1000 * 60 * mm
}

export const getElapsedTimeHM = (startedAt, stoppedAt) => {
  const started = getMillisecondsFromHM(startedAt)
  const stopped = getMillisecondsFromHM(stoppedAt)
  const elapsed = stopped - started
  if (elapsed < 0) {
    return 1000 * 60 * 60 * 24 + elapsed
  }
  return elapsed
}

export const getDurationStamp = (elapsed) => {
  if (!elapsed) {
    return '00:00:00'
  }

  const seconds = Math.floor((elapsed / 1000) % 60)
  const minutes = Math.floor((elapsed / (60 * 1000)) % 60)
  const hours = Math.floor((elapsed / (60 * 60 * 1000)) % 60)

  return String(hours).padStart(2, '0') + ':' +
    String(minutes).padStart(2, '0') + ':' +
    String(seconds).padStart(2, '0')
}

export const millisecondsToHM = (milliseconds) => {
  const date = new Date(milliseconds)
  const hh = String(date.getHours()).padStart(2, '0')
  const mm = String(date.getMinutes()).padStart(2, '0')
  return `${hh}:${mm}`
}

export const getFromToStamp = (date, duration) => {
  const from = millisecondsToHM(date)
  const to = millisecondsToHM(date + duration)
  return `${from}-${to}`
}

export const sortByDate = entries => (
  [...entries].sort((a, b) => {
    if (a.date < b.date) {
      return 1
    } else if (a.date > b.date) {
      return -1
    }
    return 0
  })
)

export const groupByDate = entries => (
  entries.reduce((acc, n) => {
    const year = new Date(n.date).getFullYear()
    const month = new Date(n.date).getMonth()
    const date = new Date(n.date).getDate()
    const key = String(date).padStart(2, '0') + '/' +
      String(month + 1).padStart(2, '0') + '/' + String(year).slice(-2)
    if (!acc.has(key)) {
      acc.set(key, [])
    }
    acc.get(key).push(n)
    return acc
  }, new Map())
)

export const filterByKey = (key, array) => {
  if (!key) {
    return array
  }

  const keyLowerCase = key.toLowerCase()
  return array.filter((object) => {
    const found = object.name
      .toLowerCase()
      .search(keyLowerCase) !== -1
    return found
  })
}

export const toggleElement = (id, array = []) => {
  const found = array.find(element => element === id)
  if (found) {
    return array.filter(element => element !== id)
  }

  return [
    id,
    ...array,
  ]
}

export const getNameByID = (id, array) => {
  const object = array.find(element => element.id === id)
  if (object) {
    return object.name
  }
  return ''
}

export const groupByClient = (projects, clients) => {
  const group = projects.reduce((group, project) => {
    group[project.client] = group[project.client] || []
    group[project.client].push(project)
    return group
  }, {})
  return Object.keys(group).map(key => ({
    client: getNameByID(parseInt(key, 10), clients) || 'No client',
    projects: group[key],
  }))
}

export const filterClientGroupByKey = (key, group) => {
  if (!key) {
    return group
  }

  return group
    .map(item => ({
      client: item.client,
      projects: filterByKey(key, item.projects),
    }))
    .filter(item => item.projects.length)
}

export const wrap = (x, min, max) =>
  x < min ? (x - min) + max + 1 : ((x > max) ? (x - max) + min - 1 : x)

export const ijToIndex = (i, j, size) => i * size + j

export const deleteMapIdValue = (id, map) => {
  const key = id.toString()
  return Object
    .keys(map)
    .reduce((acc, k) => {
      if (key !== k) {
        acc[k] = map[k]
      }
      return acc
    }, {})
}

export const getSelectedDate = (value, year, month) => {
  if (typeof value === 'string') {
    return parseInt(value.split(' ')[2], 10)
  }
  const date = new Date(value)
  if (year === date.getFullYear() && month === date.getMonth()) {
    return new Date(date).getDate()
  }
  return -1
}
