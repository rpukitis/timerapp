import React from 'react'
import renderer from 'react-test-renderer'
import { shallow, mount } from 'enzyme'
import 'jest-styled-components'
import {
  Background,
  Wrapper,
  Header,
  Title,
  Body,
  AddEntry,
  EntryInput,
  AddEntryButton,
  Entries,
  Entry,
  IcoPencil,
  IcoTimes,
  EditEntryInput,
} from './Taxonomy.styles'
import Taxonomy, { Add, List, Edit } from './Taxonomy'

describe('Taxonomy.styles', () => {
  it('should render Background styles', () => {
    const tree = renderer.create(<Background />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render Wrapper styles', () => {
    const tree = renderer.create(<Wrapper />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render Header styles', () => {
    const tree = renderer.create(<Header />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render Title styles', () => {
    const tree = renderer.create(<Title />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render Body styles', () => {
    const tree = renderer.create(<Body />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render AddEntry styles', () => {
    const tree = renderer.create(<AddEntry />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render EntryInput styles', () => {
    const tree = renderer.create(<EntryInput />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render AddEntryButton styles', () => {
    const tree = renderer.create(<AddEntryButton />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render Entries styles', () => {
    const tree = renderer.create(<Entries />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render Entry styles', () => {
    const tree = renderer.create(<Entry />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render IcoPencil styles', () => {
    const tree = renderer.create(<IcoPencil />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render IcoTimes styles', () => {
    const tree = renderer.create(<IcoTimes />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render EditEntryInput styles', () => {
    const tree = renderer.create(<EditEntryInput />).toJSON()
    expect(tree).toMatchSnapshot()
  })
})

describe('Taxonomy component', () => {
  it('should render Add', () => {
    const tree = renderer.create(
      <Add
        type="Taxonomy"
        value=""
        handleChange={() => undefined}
      />
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should handle Add input change', () => {
    const handle = jest.fn()
    const wrapper = shallow(
      <Add
        type="Taxonomy"
        value=""
        handleChange={handle}
      />
    )
    wrapper.find('[data-test="entryInput"]')
      .simulate('change', { target: { value: 'input' } })
    expect(handle).toBeCalled()
  })

  it('should handle Add field keydown', () => {
    const handle = jest.fn()
    const wrapper = mount(
      <Add
        type={'Taxonomy'}
        value=""
        handleChange={nop}
        handleKeyDown={handle}
        handleEdit={nop}
      />
    )
    wrapper.find('[data-test="entryInput"]').last()
      .simulate('keyDown', { key: 'Enter', keyCode: 13, which: 13 })
    expect(handle).toBeCalled()
  })

  it('should handle Add button click', () => {
    const handle = jest.fn()
    const wrapper = shallow(
      <Add
        type="Taxonomy"
        value=""
        handleAdd={handle}
      />
    )
    wrapper.find('[data-test="entryAddButton"]').simulate('click')
    expect(handle).toBeCalled()
  })

  const nop = () => undefined
  const listEntries = [
    { id: 1, name: 'foo' },
    { id: 2, name: 'bar' },
    { id: 3, name: 'zap' },
  ]

  it('should render List', () => {
    const tree = renderer.create(
      <List
        entries={listEntries}
        handleEdit={nop}
        handleRemove={nop}
      />
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should handle List item edit click', () => {
    const handle = jest.fn()
    const wrapper = shallow(
      <List
        entries={listEntries}
        handleEdit={handle}
        handleRemove={nop}
      />
    )
    wrapper.find('[data-test="entryEdit"]').forEach((node) => {
      node.simulate('click')
    })
    expect(handle).toHaveBeenCalledTimes(3)
  })

  it('should handle List item remove click', () => {
    const handle = jest.fn()
    const wrapper = shallow(
      <List
        entries={listEntries}
        handleEdit={nop}
        handleRemove={handle}
      />
    )
    wrapper.find('[data-test="entryDelete"]').forEach((node) => {
      node.simulate('click')
    })
    expect(handle).toHaveBeenCalledTimes(3)
  })

  it('should render Edit', () => {
    const tree = renderer.create(
      <Edit
        type={'Taxonomy'}
        value=""
        handleChange={nop}
        handleKeyDown={nop}
        handleEdit={nop}
      />
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should handle Edit field change', () => {
    const handle = jest.fn()
    const wrapper = mount(
      <Edit
        type={'Taxonomy'}
        value=""
        handleChange={handle}
        handleKeyDown={nop}
        handleEdit={nop}
      />
    )
    wrapper.find('[data-test="editEntryInput"]').last()
      .simulate('change', { target: { value: 'input' } })
    expect(handle).toBeCalled()
  })

  it('should handle Edit field keydown', () => {
    const handle = jest.fn()
    const wrapper = mount(
      <Edit
        type={'Taxonomy'}
        value=""
        handleChange={nop}
        handleKeyDown={handle}
        handleEdit={nop}
      />
    )
    wrapper.find('[data-test="editEntryInput"]').last()
      .simulate('keyDown', { key: 'Enter', keyCode: 13, which: 13 })
    expect(handle).toBeCalled()
  })

  it('should handle Edit confirm', () => {
    const handle = jest.fn()
    const wrapper = mount(
      <Edit
        type={'Taxonomy'}
        value=""
        handleChange={nop}
        handleKeyDown={nop}
        handleEdit={handle}
      />
    )
    wrapper.find('[data-test="addEntryButton"]').last().simulate('click')
    expect(handle).toBeCalled()
  })

  it('should render Taxonomy', () => {
    const tree = renderer.create(
      <Taxonomy
        type={'Taxonomy'}
        entries={listEntries}
        onAdd={nop}
        onRemove={nop}
        onUpdate={nop}
      />
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render Taxonomy in edit mode', () => {
    const wrapper = mount(
      <Taxonomy
        type={'Taxonomy'}
        entries={listEntries}
        onAdd={nop}
        onRemove={nop}
        onUpdate={nop}
      />
    )
    wrapper.find('[data-test="entryEdit"]').first().simulate('click')
    expect(wrapper).toMatchSnapshot()
  })

  it('should handle new item creation', () => {
    const handle = jest.fn()
    const wrapper = mount(
      <Taxonomy
        type={'Taxonomy'}
        entries={listEntries}
        onAdd={handle}
        onRemove={nop}
        onUpdate={nop}
      />
    )
    wrapper.find('[data-test="entryInput"]').last()
      .simulate('change', { target: { value: 'new item' } })
    wrapper.find('[data-test="entryAddButton"]').first().simulate('click')
    expect(handle).toBeCalledWith('new item')
  })

  it('should handle new item creation with keydown', () => {
    const handle = jest.fn()
    const wrapper = mount(
      <Taxonomy
        type={'Taxonomy'}
        entries={listEntries}
        onAdd={handle}
        onRemove={nop}
        onUpdate={nop}
      />
    )
    wrapper.find('[data-test="entryInput"]').last()
      .simulate('change', { target: { value: 'new item' } })
    wrapper.find('[data-test="entryInput"]').last()
      .simulate('keyDown', { key: 'Enter', keyCode: 13, which: 13 })
    expect(handle).toBeCalledWith('new item')
  })

  it('should handle item removal', () => {
    const handle = jest.fn()
    const wrapper = mount(
      <Taxonomy
        type={'Taxonomy'}
        entries={listEntries}
        onAdd={nop}
        onRemove={handle}
        onUpdate={nop}
      />
    )
    wrapper.find('[data-test="entryDelete"]').first().simulate('click')
    expect(handle).toBeCalled()
  })

  it('should open modal', () => {
    const wrapper = mount(
      <Taxonomy
        type={'Taxonomy'}
        entries={listEntries}
        onAdd={nop}
        onRemove={nop}
        onUpdate={nop}
      />
    )
    wrapper.find('[data-test="entryEdit"]').first().simulate('click')
    expect(wrapper.find('[data-test="taxonomyModal"]').length).toBeGreaterThan(0)
  })

  it('should open modal with item name in field', () => {
    const wrapper = mount(
      <Taxonomy
        type={'Taxonomy'}
        entries={listEntries}
        onAdd={nop}
        onRemove={nop}
        onUpdate={nop}
      />
    )
    wrapper.find('[data-test="entryEdit"]').first().simulate('click')
    expect(wrapper.find('[data-test="editEntryInput"]').last().props().value)
      .toBe(listEntries[0].name)
  })

  it('should open modal change item name and confirm with click', () => {
    const handle = jest.fn()
    const wrapper = mount(
      <Taxonomy
        type={'Taxonomy'}
        entries={listEntries}
        onAdd={nop}
        onRemove={nop}
        onUpdate={handle}
      />
    )
    wrapper.find('[data-test="entryEdit"]').first().simulate('click')
    wrapper.find('[data-test="editEntryInput"]').last()
      .simulate('change', { target: { value: 'new item' } })
    wrapper.find('[data-test="addEntryButton"]').first().simulate('click')
    expect(handle).toBeCalledWith(1, 'new item')
  })

  it('should open modal change item name and confirm with keydown', () => {
    const handle = jest.fn()
    const wrapper = mount(
      <Taxonomy
        type={'Taxonomy'}
        entries={listEntries}
        onAdd={nop}
        onRemove={nop}
        onUpdate={handle}
      />
    )
    wrapper.find('[data-test="entryEdit"]').first().simulate('click')
    wrapper.find('[data-test="editEntryInput"]').last()
      .simulate('change', { target: { value: 'new item' } })
    wrapper.find('[data-test="editEntryInput"]').last()
      .simulate('keyDown', { key: 'Enter', keyCode: 13, which: 13 })
    expect(handle).toBeCalledWith(1, 'new item')
  })

  it('should close modal', () => {
    const wrapper = mount(
      <Taxonomy
        type={'Taxonomy'}
        entries={listEntries}
        onAdd={nop}
        onRemove={nop}
        onUpdate={nop}
      />
    )
    wrapper.find('[data-test="entryEdit"]').first().simulate('click')
    wrapper.find('[data-test="modalClose"]').first().simulate('click')
    expect(wrapper.find('[data-test="taxonomyModal"]')).toHaveLength(0)
  })
})
