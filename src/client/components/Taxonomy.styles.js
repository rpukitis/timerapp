import styled from 'styled-components'

export const Background = styled.div`
  margin: 25px 35px;
`

export const Wrapper = styled.div`
  background-color: #fff;
  box-shadow: 0 5px 15px rgba(128,128,128,.5);
`

export const Header = styled.div`
  display: flex;
  align-items: center;
  height: 75px;
  border-bottom: 1px solid #ebebeb;
`

export const Title = styled.h1`
  margin-left: 40px;
  font-size: 32px;
  font-weight: 400;
`

export const Body = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 40px 40px;
`

export const AddEntry = styled.div`
  display: flex;
  margin-bottom: 40px;
`

export const EntryInput = styled.input`
  width: 290px;
  height: 40px;
  padding: 18px 10px;
  font-size: 14px;

  :focus {
    outline: 3px auto #ddd;
  }
`

export const AddEntryButton = styled.button`
  width: 145px;
  height: 40px;
  font-size: 14px;
  font-weight: 600;
  color: #fff;
  background-color: #4BC800;
  box-shadow: inset 0 -2px 0 #45b900;
  cursor: pointer;

  ':hover': {
    background-color: #43b400;
  },
`

export const Entries = styled.div`
  display: flex;
  flex-wrap: wrap;
  width: 100%;
`

export const Entry = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  height: 25px;
  margin: 0 10px 10px 0;
  padding: 0 10px;
  font-size: 11px;
  font-weight: 600;
  color: #595959;
  border-radius: 2px;
  background-color: #e6e6e6;
`

export const IcoButton = styled.i`
  color: #a6a6a6;
  cursor: pointer;

  :hover {
    color: #595959;
  }
`

export const IcoPencil = IcoButton.extend.attrs({
  className: 'fa fa-pencil',
})`
  padding: 0 7px;
  margin-left: 5px;
`

export const IcoTimes = IcoButton.extend.attrs({
  className: 'fa fa-times',
})`
  padding-left: 5px;
  margin-left: 1px;
  border-left: 1px solid #bbb;
`

export const EditEntryInput = EntryInput.extend`
  width: 100%;
  margin-bottom: 20px;
`
