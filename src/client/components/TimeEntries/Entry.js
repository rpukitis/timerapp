import React from 'react'
import { getDurationStamp, getFromToStamp } from '../../services'
import {
  Entry as EntryStyle,
  Description,
  Project,
  ProjectColor,
  ClientName,
  Tag,
  FromTo,
  Duration,
  StartButton,
  DeleteButton,
} from './Entry.styles'

const Entry = (props) => {
  const {
    description,
    projects,
    clients,
    tags,
    date,
    duration,
    startTimer,
    removeEntry,
  } = props
  const project = projects[props.project]
  const client = project && clients[project.client]

  return (
    <EntryStyle>
      <Description>{description}</Description>
      {project &&
        <Project>
          <ProjectColor style={{ backgroundColor: project.color }} />
          <div style={{ color: project.color }}> {project.name} </div>
          {client &&
            <ClientName> {client.name} </ClientName>
          }
        </Project>
      }
      <Tag highlighted={tags && tags.length} />
      <FromTo> {getFromToStamp(date, duration)} </FromTo>
      <Duration> {getDurationStamp(duration)} </Duration>
      <StartButton data-test="startButton" onClick={startTimer} />
      <DeleteButton data-test="deleteButton" onClick={removeEntry} />
    </EntryStyle>
  )
}

export default Entry
