import React from 'react'
import renderer from 'react-test-renderer'
import { mount } from 'enzyme'
import 'jest-styled-components'
import configureMockStore from 'redux-mock-store'
import { Provider } from 'react-redux'
import thunk from 'redux-thunk'
import { test as state } from '../../store/state'
import { Wrapper } from './index.styles'
import TimeEntries from './index'

const mockStore = configureMockStore([thunk])
const services = require.requireActual('../../services')
services.getFromToStamp = jest.fn(() => '12:00-12:45')
services.getDurationStamp = jest.fn(() => '00:45:00')

describe('TimeEntries/index component', () => {
  it('should render Wrapper styles', () => {
    const tree = renderer.create(<Wrapper />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render TimeEntries component', () => {
    const tree = renderer.create(
      <Provider store={mockStore(state)}>
        <TimeEntries />
      </Provider>
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should handle start timer', () => {
    const getTime = Date.prototype.getTime
    Date.prototype.getTime = () => 1000 // eslint-disable-line
    const store = mockStore(state)
    const wrapper = mount(
      <Provider store={store}>
        <TimeEntries />
      </Provider>
    )
    wrapper.find('[data-test="startButton"]').first().simulate('click')
    expect(store.getActions()).toEqual([
      { type: 'UPDATE_TIMER_PROJECT', project: -100 },
      { type: 'UPDATE_TIMER_DESCRIPTION', description: 'developing awesome app' },
      { type: 'UPDATE_TIMER_TAGS', tags: undefined },
      { type: 'START_TIMER', now: 1000 },
    ])
    Date.prototype.getTime = () => getTime // eslint-disable-line
  })

  it('should handle start timer', () => {
    const getTime = Date.prototype.getTime
    Date.prototype.getTime = () => 1000 // eslint-disable-line
    const store = mockStore(state)
    const wrapper = mount(
      <Provider store={store}>
        <TimeEntries />
      </Provider>
    )
    wrapper.find('[data-test="deleteButton"]').first().simulate('click')
    expect(store.getActions()).toEqual([{ type: 'REMOVE_ENTRY', id: -100 }])
    Date.prototype.getTime = () => getTime // eslint-disable-line
  })
})
