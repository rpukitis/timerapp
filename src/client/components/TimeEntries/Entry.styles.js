import styled, { css } from 'styled-components'
import { tagIcon, tagIconHover, startIcon, deleteIcon } from '../../assets/icons'

export const Entry = styled.li`
  display: flex;
  align-items: center;
  height: 60px;
  padding: 0 30px;
  background-color: #fff;
  box-shadow: inset 0 -1px 0 0 #eceded;
  list-style-type: none;
`

export const Description = styled.div`
  flex: 0 1 auto;
  min-width: 165px;
  max-width: 25%;
  font-size: 14px;
  line-height: 1.2;
  overflow: hidden;
`
export const Project = styled.div`
  flex: 0 1 auto;
  display: flex;
  align-items: center;
  margin-left: 15px;
  fon-size: 15px;
`

export const ProjectColor = styled.div`
  display: inline-block;
  width: 12px;
  height: 12px;
  border-radius: 50%;
  margin-right: 9px;
`

export const ClientName = styled.div`
  color: #a3a3a3;

  ::before {
    display: inline-block;
    content: '\\2022';
    margin: 0 7px;
    font-family: Arial;
    font-size: 16px;
    font-weight: 700;
    color: #cecece;
    vertical-align: middle;
  }
`

export const Tag = styled.div`
  width: 36px;
  height: 36px;
  margin-left: auto;
  margin-right: 50px;
  background: url(${tagIcon}) no-repeat center center;

  ${props => props.highlighted && css`
    background: url(${tagIconHover}) no-repeat center center;
  `}
`

export const FromTo = styled.span`
  margin-right: 10px;
  font-size: 11px;
  color: #a3a3a3;
`

export const Duration = styled.span`
  margin-right: 10px;
  font-size: 14px;
  color: #222;
`

export const StartButton = styled.div`
  width: 32px;
  height: 32px;
  cursor: pointer;
  background: url(${startIcon}) no-repeat 50%;
`

export const DeleteButton = styled.div`
  width: 32px;
  height: 32px;
  cursor: pointer;
  background: url(${deleteIcon}) no-repeat 50%;
`
