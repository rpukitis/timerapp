import React from 'react'
import Entry from './Entry'
import { Group as GroupStyle, Date } from './Group.styles'

const Group = ({ date, entries, projects, clients, startTimer, removeEntry }) => (
  <GroupStyle>
    <Date key="date">{date}</Date>
    {entries.map(entry =>
      <Entry
        key={entry.id}
        {...entry}
        projects={projects}
        clients={clients}
        startTimer={() => startTimer(entry.project, entry.description, entry.tags)}
        removeEntry={() => removeEntry(entry.id)}
      />
    )}
  </GroupStyle>
)
export default Group
