import React from 'react'
import { connect } from 'react-redux'
import { removeEntry } from '../../store/entries'
import { startTimerFromEntry } from '../../store/timer'
import {
  getProjects,
  getClients,
  getEntriesGroupedByDate,
  getEntryGroup,
} from '../../store/selectors'
import Group from './Group'
import { Wrapper } from './index.styles'

const Entries = ({ entries, group, projects, clients, startTimer, removeEntry }) => (
  <Wrapper>
    {group.map(date =>
      <Group
        key={date + entries.get(date).length}
        date={date}
        entries={entries.get(date)}
        projects={projects}
        clients={clients}
        startTimer={startTimer}
        removeEntry={removeEntry}
      />
    )}
  </Wrapper>
)

const mapStateToProps = state => ({
  entries: getEntriesGroupedByDate(state),
  group: getEntryGroup(state),
  projects: getProjects(state),
  clients: getClients(state),
})

export default connect(mapStateToProps, {
  startTimer: startTimerFromEntry,
  removeEntry,
})(Entries)
