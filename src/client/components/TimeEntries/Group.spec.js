import React from 'react'
import renderer from 'react-test-renderer'
import 'jest-styled-components'
import { test as state } from '../../store/state'
import { Group as GroupStyle, Date } from './Group.styles'
import Group from './Group'

jest.mock('../../services', () => ({
  getFromToStamp: () => '12:00-12:45',
  getDurationStamp: () => '00:45:00',
}))

describe('TimeEntries/Group component', () => {
  it('should render GroupStyle styles', () => {
    const tree = renderer.create(<GroupStyle />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render Date styles', () => {
    const tree = renderer.create(<Date />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render Group', () => {
    const tree = renderer.create(
      <Group
        date="17/12/2017"
        entries={Object.values(state.entries)}
        projects={state.projects}
        clients={state.projects}
      />
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })
})
