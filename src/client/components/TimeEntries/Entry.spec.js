import React from 'react'
import renderer from 'react-test-renderer'
import { shallow } from 'enzyme'
import 'jest-styled-components'
import { test as state } from '../../store/state'
import {
  Entry as EntryStyle,
  Description,
  Project,
  ProjectColor,
  ClientName,
  Tag,
  FromTo,
  Duration,
  StartButton,
  DeleteButton,
} from './Entry.styles'
import Entry from './Entry'

jest.mock('../../services', () => ({
  getFromToStamp: () => '12:00-12:45',
  getDurationStamp: () => '00:45:00',
}))

describe('TimeEntries/Entry.styles', () => {
  it('should render EntryStyle styles', () => {
    const tree = renderer.create(<EntryStyle />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render Description styles', () => {
    const tree = renderer.create(<Description />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render Project styles', () => {
    const tree = renderer.create(<Project />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render ProjectColor styles', () => {
    const tree = renderer.create(<ProjectColor />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render ClientName styles', () => {
    const tree = renderer.create(<ClientName />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render Tag styles', () => {
    const tree = renderer.create(<Tag />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render FromTo styles', () => {
    const tree = renderer.create(<FromTo />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render Duration styles', () => {
    const tree = renderer.create(<Duration />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render StartButton styles', () => {
    const tree = renderer.create(<StartButton />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render DeleteButton styles', () => {
    const tree = renderer.create(<DeleteButton />).toJSON()
    expect(tree).toMatchSnapshot()
  })
})

describe('TimeEntries/Entry component', () => {
  const entry = Object.values(state.entries)[0]

  it('should render Entry', () => {
    const tree = renderer.create(
      <Entry {...entry} projects={state.projects} clients={state.clients} />
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should handle start timer', () => {
    const handle = jest.fn()
    const wrapper = shallow(
      <Entry
        {...entry}
        projects={state.projects}
        clients={state.clients}
        startTimer={handle}
      />
    )
    wrapper.find('[data-test="startButton"]').simulate('click')
    expect(handle).toHaveBeenCalled()
  })

  it('should handle delete entry', () => {
    const handle = jest.fn()
    const wrapper = shallow(
      <Entry
        {...entry}
        projects={state.projects}
        clients={state.clients}
        removeEntry={handle}
      />
    )
    wrapper.find('[data-test="deleteButton"]').simulate('click')
    expect(handle).toHaveBeenCalled()
  })
})
