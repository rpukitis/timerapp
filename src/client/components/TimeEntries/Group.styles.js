import styled from 'styled-components'

export const Group = styled.div`
  margin-bottom: 30px;
`

export const Date = styled.div`
  display: flex;
  align-items: center;
  height: 60px;
  padding: 0 30px;
  box-shadow: inset 0 -1px 0 0 #eceded;
  list-style-type: none;
  font-size: 18px;
  color: #222;
`
