import styled from 'styled-components'
import { clockIcon, folderIcon, usersIcon, tagsIcon } from '../assets/icons'

export const Wrapper = styled.div`
  display: flex;
`

export const Sidebar = styled.div`
  width: 100px;
`

export const Menu = styled.ul`
  position: fixed;
  width: 100px;
  height: 100vh;
  padding: 12px 20px;
  background: #323232;
  list-style-type: none;
`

export const MenuItem = styled.div`
  width: 50px;
  height: 50px;
  margin-bottom: 50px;
  background-repeat: no-repeat;
  background-position: center center;
`

export const TimerItem = MenuItem.extend`
  background-image: url(${clockIcon});
  background-size: 38px;
`

export const ProjectItem = MenuItem.extend`
  background-image: url(${folderIcon});
  background-size: 34px;
`

export const ClientItem = MenuItem.extend`
  background-image: url(${usersIcon});
  background-size: 32px;
`

export const TagsItem = MenuItem.extend`
  background-image: url(${tagsIcon});
  background-size: 32px;
`

export const Content = styled.div`
  flex: 1 0;
`
