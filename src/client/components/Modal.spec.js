import React from 'react'
import renderer from 'react-test-renderer'
import { shallow } from 'enzyme'
import 'jest-styled-components'
import { Wrapper, Header, Title, Close, Body, Background } from './Modal.styles'
import Modal from './Modal'

describe('Modal component', () => {
  it('should render Wrapper styles', () => {
    const tree = renderer.create(<Wrapper />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render Header styles', () => {
    const tree = renderer.create(<Header />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render Title styles', () => {
    const tree = renderer.create(<Title />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render Close styles', () => {
    const tree = renderer.create(<Close />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render Body styles', () => {
    const tree = renderer.create(<Body />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render Background styles', () => {
    const tree = renderer.create(<Background />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render Modal component', () => {
    const tree = renderer.create(
      <Modal title="Test Modal">
        <div>!!!Test Modal!!!</div>
      </Modal>
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render small Modal component', () => {
    const tree = renderer.create(
      <Modal title="Test Modal" small>
        <div>!!!Test Modal!!!</div>
      </Modal>
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should handle close', () => {
    const handle = jest.fn()
    const wrapper = shallow(
      <Modal title="Test Modal" onClose={handle}>
        <div>!!!Test Modal!!!</div>
      </Modal>
    )
    wrapper.find('[data-test="modalClose"]').simulate('click')
    expect(handle).toBeCalled()
  })
})
