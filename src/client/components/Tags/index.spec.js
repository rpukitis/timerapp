import React from 'react'
import renderer from 'react-test-renderer'
import { mount } from 'enzyme'
import 'jest-styled-components'
import configureMockStore from 'redux-mock-store'
import { Provider } from 'react-redux'
import thunk from 'redux-thunk'
import state from '../../store/state'
import { addNextTagID } from '../../store/tags'
import Tags from './'

const mockStore = configureMockStore([thunk])

describe('Tags/index component', () => {
  it('should render Tags', () => {
    const tree = renderer.create(
      <Provider store={mockStore(state)}>
        <Tags />
      </Provider>
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should create new tag', () => {
    const store = mockStore(state)
    const wrapper = mount(
      <Provider store={store}>
        <Tags />
      </Provider>
    )
    wrapper.find('[data-test="entryInput"]').last()
      .simulate('change', { target: { value: 'awesome tag' } })
    wrapper.find('[data-test="entryAddButton"]').first().simulate('click')
    expect(store.getActions()).toEqual([
      { type: 'ADD_TAG', id: addNextTagID - 1, name: 'awesome tag' },
    ])
  })

  it('should delete tag', () => {
    const store = mockStore(state)
    const wrapper = mount(
      <Provider store={store}>
        <Tags />
      </Provider>
    )
    wrapper.find('[data-test="entryDelete"]').first().simulate('click')
    expect(store.getActions()).toEqual([
      { type: 'REMOVE_TAG', id: -100 },
    ])
  })

  it('should edit tag', () => {
    const store = mockStore(state)
    const wrapper = mount(
      <Provider store={store}>
        <Tags />
      </Provider>
    )
    wrapper.find('[data-test="entryEdit"]').first().simulate('click')
    wrapper.find('[data-test="editEntryInput"]').last()
      .simulate('change', { target: { value: 'awesome tag' } })
    wrapper.find('[data-test="addEntryButton"]').first().simulate('click')
    expect(store.getActions()).toEqual([
      { type: 'UPDATE_TAG', id: -100, name: 'awesome tag' },
    ])
  })
})
