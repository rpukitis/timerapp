import React from 'react'
import { connect } from 'react-redux'
import * as actions from '../../store/tags'
import { getTagArray } from '../../store/selectors'
import Taxonomy from '../Taxonomy'

const Tags = ({ tags, addTag, updateTag, removeTag }) => (
  <Taxonomy
    type={'Tag'}
    entries={tags}
    onAdd={addTag}
    onUpdate={updateTag}
    onRemove={removeTag}
  />
)

const mapStateToProps = state => ({
  tags: getTagArray(state),
})

const {
  addTag,
  updateTag,
  removeTag,
} = actions

const mapDispatchToProps = {
  addTag,
  updateTag,
  removeTag,
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Tags)
