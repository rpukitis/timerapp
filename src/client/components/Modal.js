import React from 'react'
import { Wrapper, Header, Title, Close, Body, Background } from './Modal.styles'

const Modal = ({ title, small, onClose, children }) => (
  <Background data-test="modal">
    <Wrapper small={small}>
      <Header>
        <Title>{title}</Title>
        <Close data-test="modalClose" onClick={onClose} />
      </Header>
      <Body small={small}> {children} </Body>
    </Wrapper>
  </Background>
)

export default Modal
