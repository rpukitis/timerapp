import styled, { css, keyframes } from 'styled-components'

const translateYKeyframes = keyframes`
  from {
    transform: translateY(calc(-100vh - 25%));
  }

  to {
    transform: translateY(calc(-15vh));
  }
`

export const Wrapper = styled.div`
  width: 760px;
  transform: translateY(calc(-15vh));
  background-color: #f5f5f5;
  box-shadow: 0 5px 15px rgba(128,128,128,.5);
  animation: ${translateYKeyframes} 0.5s;

  ${props => props.small && css`
    width: 520px;
  `}
`

export const Header = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  height: 60px;
  padding: 0 30px 0 30px;
  box-shadow: 0 1px 3px rgba(128,128,128,.2);
`

export const Title = styled.h2`
  font-size: 16px;
  font-weight: '600';
`

export const Close = styled.i.attrs({
  className: 'fa fa-times',
})`
  font-size: 20px;
  color: #666;
  cursor: pointer;
`

export const Body = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  height: 190px;
  padding: 30px 30px;

  ${props => props.small && css`
    align-items: flex-end;
    height: auto;
  `}
`

export const Background = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100vh;
  background-color: rgba(128, 128, 128, 0.5);
`
