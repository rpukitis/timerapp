import React from 'react'
import renderer from 'react-test-renderer'
import { mount } from 'enzyme'
import 'jest-styled-components'
import configureMockStore from 'redux-mock-store'
import { Provider } from 'react-redux'
import thunk from 'redux-thunk'
import state from '../../store/state'
import { addClientNextID } from '../../store/clients'
import Client from './'

const mockStore = configureMockStore([thunk])

describe('Client/index component', () => {
  it('should render Client', () => {
    const tree = renderer.create(
      <Provider store={mockStore(state)}>
        <Client />
      </Provider>
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should create new client', () => {
    const store = mockStore(state)
    const wrapper = mount(
      <Provider store={store}>
        <Client />
      </Provider>
    )
    wrapper.find('[data-test="entryInput"]').last()
      .simulate('change', { target: { value: 'awesome client' } })
    wrapper.find('[data-test="entryAddButton"]').first().simulate('click')
    expect(store.getActions()).toEqual([
      { type: 'ADD_CLIENT', id: addClientNextID - 1, name: 'awesome client' },
    ])
  })

  it('should delete client', () => {
    const store = mockStore(state)
    const wrapper = mount(
      <Provider store={store}>
        <Client />
      </Provider>
    )
    wrapper.find('[data-test="entryDelete"]').first().simulate('click')
    expect(store.getActions()).toEqual([
      { type: 'REMOVE_CLIENT', id: -100 },
    ])
  })

  it('should edit client', () => {
    const store = mockStore(state)
    const wrapper = mount(
      <Provider store={store}>
        <Client />
      </Provider>
    )
    wrapper.find('[data-test="entryEdit"]').first().simulate('click')
    wrapper.find('[data-test="editEntryInput"]').last()
      .simulate('change', { target: { value: 'awesome client' } })
    wrapper.find('[data-test="addEntryButton"]').first().simulate('click')
    expect(store.getActions()).toEqual([
      { type: 'UPDATE_CLIENT', id: -100, name: 'awesome client' },
    ])
  })
})
