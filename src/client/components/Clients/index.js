import React from 'react'
import { connect } from 'react-redux'
import * as actions from '../../store/clients'
import { getClientArray } from '../../store/selectors'
import Taxonomy from '../Taxonomy'

const Clients = ({ clients, addClient, updateClient, removeClient }) => (
  <Taxonomy
    type={'Client'}
    entries={clients}
    onAdd={addClient}
    onUpdate={updateClient}
    onRemove={removeClient}
  />
)

const mapStateToProps = state => ({
  clients: getClientArray(state),
})

const {
  addClient,
  updateClient,
  removeClient,
} = actions

const mapDispatchToProps = {
  addClient,
  updateClient,
  removeClient,
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Clients)
