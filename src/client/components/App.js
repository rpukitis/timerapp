import React from 'react'
import { Provider } from 'react-redux'
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'
import Timer from './Timer'
import TimeEntries from './TimeEntries'
import Projects from './Projects'
import Clients from './Clients'
import Tags from './Tags'
import {
  Wrapper,
  Sidebar,
  Menu,
  TimerItem,
  ProjectItem,
  ClientItem,
  TagsItem,
  Content,
} from './App.styles'

const AppHome = () => (
  <div>
    <Timer />
    <TimeEntries />
  </div>
)

const routes = [
  {
    path: '/',
    exact: true,
    content: () => <AppHome />,
  },
  {
    path: '/projects',
    content: () => <Projects />,
  },
  {
    path: '/clients',
    content: () => <Clients />,
  },
  {
    path: '/tags',
    content: () => <Tags />,
  },
]

const App = ({ store }) => (
  <Provider store={store}>
    <Router>
      <Wrapper>
        <Sidebar>
          <Menu>
            <li> <Link to="/" data-test="toTimer"> <TimerItem /> </Link> </li>
            <li> <Link to="/projects" data-test="toProject"> <ProjectItem /> </Link> </li>
            <li> <Link to="/clients" data-test="toClients"> <ClientItem /> </Link> </li>
            <li> <Link to="/tags" data-test="toTags"> <TagsItem /> </Link> </li>
          </Menu>
        </Sidebar>
        <Content>
          {routes.map((route, index) => (
            <Route
              key={index} // eslint-disable-line
              path={route.path}
              exact={route.exact}
              component={route.content}
            />
          ))}
        </Content>
      </Wrapper>
    </Router>
  </Provider>
)
export default App
