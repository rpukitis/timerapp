import React from 'react'
import renderer from 'react-test-renderer'
import { mount } from 'enzyme'
import 'jest-styled-components'
import configureMockStore from 'redux-mock-store'
import state from '../store/state'
import {
  Wrapper,
  Sidebar,
  Menu,
  TimerItem,
  ProjectItem,
  ClientItem,
  TagsItem,
  Content,
} from './App.styles'
import App from './App'

const mockStore = configureMockStore([])

describe('App.styles', () => {
  it('should render Wrapper styles', () => {
    const tree = renderer.create(<Wrapper />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render Sidebar styles', () => {
    const tree = renderer.create(<Sidebar />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render Menu styles', () => {
    const tree = renderer.create(<Menu />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render TimerItem styles', () => {
    const tree = renderer.create(<TimerItem />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render ProjectItem styles', () => {
    const tree = renderer.create(<ProjectItem />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render ClientItem styles', () => {
    const tree = renderer.create(<ClientItem />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render TagsItem styles', () => {
    const tree = renderer.create(<TagsItem />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render Content styles', () => {
    const tree = renderer.create(<Content />).toJSON()
    expect(tree).toMatchSnapshot()
  })
})

describe('App component', () => {
  it('should render App', () => {
    const wrapper = mount(<App store={mockStore(state)} />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render App projects route', () => {
    const wrapper = mount(<App store={mockStore(state)} />)
    wrapper.find('[data-test="toProject"]').last().simulate('click')
    expect(wrapper).toMatchSnapshot()
  })

  it('should render App clients route', () => {
    const wrapper = mount(<App store={mockStore(state)} />)
    wrapper.find('[data-test="toClients"]').last().simulate('click')
    expect(wrapper).toMatchSnapshot()
  })

  it('should render App tags route', () => {
    const wrapper = mount(<App store={mockStore(state)} />)
    wrapper.find('[data-test="toTags"]').last().simulate('click')
    expect(wrapper).toMatchSnapshot()
  })
})
