import React from 'react'
import { List, Header, Icon, Items, Item } from './ClientList.styles'

const ClientList = ({ clients, handleWithoutClientSelect, handleClientSelect }) => (
  <List>
    <Header>
      <Icon />
    </Header>
    <Items>
      <Item
        key="-Infinity"
        data-test="withoutClientItem"
        onClick={handleWithoutClientSelect}
      >
        Without client
      </Item>
      {clients.map(client =>
        <Item
          key={client.id}
          data-test="clientItem"
          onClick={handleClientSelect(client.id)}
        >
          {client.name}
        </Item>,
      )}
    </Items>
  </List>
)

export default ClientList
