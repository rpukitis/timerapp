import React from 'react'
import {
  compose,
  branch,
  renderComponent,
  renderNothing,
  withProps,
  withState,
  withHandlers,
} from 'recompose'
import { connect } from 'react-redux'
import { getNameByID } from '../../services'
import * as actions from '../../store/projects'
import { getProjectArray, getClientArray } from '../../store/selectors'
import Field from './Field'
import Client from './Client'
import Color from './Color'
import Modal from '../Modal'
import {
  Wrapper,
  Title,
  CreateProject,
  CreateButton,
  EditControls,
  EditButtons,
  EditDelete,
  Table as TableStyle,
  HeaderRow,
  HeaderColumn,
  ColumnSpacer,
  Column35,
  Column25,
  Row,
  Column,
  ProjectColor,
} from './index.styles'

export const COLORS = [
  '#06aaf5', '#c56bff', '#ea468d', '#fb8b14', '#c7741c',
  '#4bc800', '#04bb9b', '#e19a86', '#3750b5', '#a01aa5',
  '#f1c33f', '#205500', '#890000', '#e20505', '#000000',
]

export const Create = ({
  name,
  client,
  clients,
  color,
  handleNameChange,
  handleWithoutClientSelect,
  handleClientSelect,
  handleColorSelect,
  handleProjectCreate,
}) => (
  <CreateProject>
    <Field
      value={name}
      data-test="createField"
      onChange={handleNameChange}
    >
      <Client
        client={getNameByID(client, clients)}
        clients={clients}
        data-test="createClient"
        handleClientSelect={handleClientSelect}
        handleWithoutClientSelect={handleWithoutClientSelect}
      />
    </Field>
    <Color
      color={color}
      colors={COLORS}
      data-test="createColor"
      handleColorSelect={handleColorSelect}
    />
    <CreateButton data-test="createButton" onClick={handleProjectCreate}>
      Create
    </CreateButton>
  </CreateProject>
)

export const Edit = ({
  name,
  client,
  clients,
  color,
  handleNameChange,
  handleWithoutClientSelect,
  handleClientSelect,
  handleColorSelect,
  handleProjectEditToggle,
  handleProjectEditRemove,
  handleProjectEditUpdate,
}) => (
  <Modal
    title={'Edit Project'}
    data-test="editModal"
    onClose={handleProjectEditToggle(null)}
  >
    <Field
      value={name}
      data-test="editField"
      onChange={handleNameChange}
    >
      <Client
        client={getNameByID(client, clients)}
        clients={clients}
        data-test="editClient"
        handleClientSelect={handleClientSelect}
        handleWithoutClientSelect={handleWithoutClientSelect}
      />
    </Field>
    <EditControls>
      <Color
        left
        color={color}
        colors={COLORS}
        data-test="editColor"
        handleColorSelect={handleColorSelect}
      />
      <EditButtons>
        <EditDelete data-test="editDelete" onClick={handleProjectEditRemove}>
          Delete
        </EditDelete>
        <CreateButton data-test="editUpdate" onClick={handleProjectEditUpdate}>
          Update
        </CreateButton>
      </EditButtons>
    </EditControls>
  </Modal>
)

const enhance = compose(
  withState('name', 'setName', ({ name = '' }) => name),
  withState('client', 'setClient', ({ client = 0 }) => client),
  withState('color', 'setColor', ({ color = COLORS[0] }) => color),
  withHandlers({
    handleNameChange: ({ setName }) => (e) => {
      setName(e.target.value)
    },
    handleWithoutClientSelect: ({ setClient }) => () => {
      setClient(0)
    },
    handleClientSelect: ({ setClient }) => (id) => () => {
      setClient(id)
    },
    handleColorSelect: ({ setColor }) => (color) => () => {
      setColor(color)
    },
  }),
)

export const ProjectCreate = compose(
  connect((state) => ({ clients: getClientArray(state) }), {
    addProject: actions.addProject,
  }),
  enhance,
  withHandlers({
    handleProjectCreate: (props) => () => {
      const { name, color, client, setName, setClient, setColor, addProject } = props
      addProject(name, color, client, 160)
      setName('')
      setClient(0)
      setColor(COLORS[0])
    },
  }),
)(Create)

const ProjectEdit = branch(({ id }) => id, compose(
  withProps(({ id, projects }) => ({
    ...projects.find(p => p.id === id),
  })),
  enhance,
  withHandlers({
    handleProjectEditUpdate: (props) => () => {
      const { id, name, color, client, setId, updateProject } = props
      updateProject(id, name, color, client)
      setId(null)
    },
    handleProjectEditRemove: ({ id, setId, removeProject }) => () => {
      removeProject(id)
      setId(null)
    },
  }),
  renderComponent(Edit),
))(renderNothing())

const Table = (props) => [
  <div key="project-table">
    <TableStyle>
      <thead>
        <HeaderRow >
          <ColumnSpacer />
          <HeaderColumn> Project </HeaderColumn>
          <Column35> Client </Column35>
          <Column25> Status </Column25>
        </HeaderRow>
      </thead>
      <tbody>
        {props.projects.map(project =>
          <Row
            key={project.id}
            data-test="openEditModal"
            onClick={props.handleProjectEditToggle(project.id)}
          >
            <td />
            <Column>
              <span>{project.name}</span>
              <ProjectColor style={{ backgroundColor: project.color }} />
            </Column>
            <Column> {getNameByID(project.client, props.clients)} </Column>
            <Column> {project.status + 'h'} </Column>
          </Row>
        )}
      </tbody>
    </TableStyle>
  </div>,
  <ProjectEdit key="project-edit" {...props} />,
]

export const ProjectTable = compose(
  connect((state) => ({
    projects: getProjectArray(state),
    clients: getClientArray(state),
  }), {
    updateProject: actions.updateProject,
    removeProject: actions.removeProject,
  }),
  withState('id', 'setId', null),
  withHandlers({
    handleProjectEditToggle: ({ setId }) => (id) => () => {
      setId(id)
    },
  }),
)(Table)

const Projects = () => (
  <Wrapper>
    <Title>Projects</Title>
    <ProjectCreate />
    <ProjectTable />
  </Wrapper>
)

export default Projects
