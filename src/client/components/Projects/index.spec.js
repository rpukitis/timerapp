import React from 'react'
import renderer from 'react-test-renderer'
import { shallow, mount } from 'enzyme'
import 'jest-styled-components'
import configureMockStore from 'redux-mock-store'
import { Provider } from 'react-redux'
import state from '../../store/state'
import { addProjectNextID } from '../../store/projects'
import {
  Wrapper,
  Title,
  CreateProject,
  CreateButton,
  EditControls,
  EditButtons,
  EditDelete,
  Table as TableStyle,
  HeaderRow,
  HeaderColumn,
  ColumnSpacer,
  Column35,
  Column25,
  Row,
  Column,
  ProjectColor,
} from './index.styles'
import Projects, {
  Create,
  Edit,
  ProjectCreate,
  ProjectTable,
  COLORS,
} from './index'

const mockStore = configureMockStore([])

describe('Project/index.styles', () => {
  it('should render Wrapper styles', () => {
    const tree = renderer.create(<Wrapper />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render Title styles', () => {
    const tree = renderer.create(<Title />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render CreateProject styles', () => {
    const tree = renderer.create(<CreateProject />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render CreateButton styles', () => {
    const tree = renderer.create(<CreateButton />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render EditControls styles', () => {
    const tree = renderer.create(<EditControls />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render EditButtons styles', () => {
    const tree = renderer.create(<EditButtons />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render EditDelete styles', () => {
    const tree = renderer.create(<EditDelete />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render TableStyle styles', () => {
    const tree = renderer.create(<TableStyle />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render HeaderRow styles', () => {
    const tree = renderer.create(<HeaderRow />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render HeaderColumn styles', () => {
    const tree = renderer.create(<HeaderColumn />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render ColumnSpacer styles', () => {
    const tree = renderer.create(<ColumnSpacer />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render Column35 styles', () => {
    const tree = renderer.create(<Column35 />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render Column25 styles', () => {
    const tree = renderer.create(<Column25 />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render Row styles', () => {
    const tree = renderer.create(<Row />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render Column styles', () => {
    const tree = renderer.create(<Column />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render ProjectColor styles', () => {
    const tree = renderer.create(<ProjectColor />).toJSON()
    expect(tree).toMatchSnapshot()
  })
})

describe('Project/index component', () => {
  it('should render Create', () => {
    const nop = () => undefined
    const tree = renderer.create(
      <Create
        name=""
        clients={Object.values(state.clients)}
        color={COLORS[0]}
        handleNameChange={nop}
        handleClientSelect={nop}
        handleColorSelect={nop}
      />
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render Edit', () => {
    const nop = () => undefined
    const projects = Object.values(state.projects)
    const project = projects[0]
    const tree = renderer.create(
      <Edit
        name={project.name}
        client={project.client}
        clients={Object.values(state.clients)}
        color={project.color}
        handleNameChange={nop}
        handleClientSelect={nop}
        handleColorSelect={nop}
        handleProjectEditToggle={nop}
      />
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })

  const setupProjectData = (wrapper, mode) => {
    wrapper
      .find(`[data-test="${mode}Field"]`)
      .find('[data-test="input"]').last()
      .simulate('change', { target: { value: 'awesome client 004' } })
    wrapper
      .find(`[data-test="${mode}Client"]`)
      .find('[data-test="clientSelect"]')
      .simulate('click')
    wrapper
      .find(`[data-test="${mode}Client"]`)
      .find('[data-test="clientSelect"]')
      .find('[data-test="clientItem"]').at(1)
      .simulate('click')
    wrapper
      .find(`[data-test="${mode}Color"]`)
      .find('[data-test="colorSelect"]').last()
      .simulate('click')
    wrapper
      .find(`[data-test="${mode}Color"]`)
      .find('[data-test="colorSelect"]').last()
      .find('[data-test="colorItem"]').at(1)
      .simulate('click')
  }

  it('should handle all user input and new project creation', () => {
    const store = mockStore(state)
    const wrapper = mount(
      <Provider store={store}>
        <ProjectCreate />
      </Provider>
    )
    setupProjectData(wrapper, 'create')
    wrapper.find('[data-test="createButton"]').last().simulate('click')
    expect(store.getActions()).toEqual([{
      type: 'ADD_PROJECT',
      id: addProjectNextID - 1,
      name: 'awesome client 004',
      color: '#06aaf5',
      client: -100,
      status: 160,
    }])
  })

  it('should handle all user input and edit project', () => {
    const store = mockStore(state)
    const wrapper = mount(
      <Provider store={store}>
        <ProjectTable />
      </Provider>
    )
    wrapper.find('[data-test="openEditModal"]').first().simulate('click')
    setupProjectData(wrapper, 'edit')
    wrapper.find('[data-test="editUpdate"]').first().simulate('click')
    expect(store.getActions()).toEqual([{
      type: 'UPDATE_PROJECT',
      id: -100,
      name: 'awesome client 004',
      color: '#06aaf5',
      client: -100,
    }])
  })

  it('should handle project delete', () => {
    const store = mockStore(state)
    const wrapper = mount(
      <Provider store={store}>
        <ProjectTable />
      </Provider>
    )
    wrapper.find('[data-test="openEditModal"]').first().simulate('click')
    setupProjectData(wrapper, 'edit')
    wrapper.find('[data-test="editDelete"]').first().simulate('click')
    expect(store.getActions()).toEqual([
      { type: 'REMOVE_PROJECT', id: -100 },
    ])
  })

  it('should handle project edit open and close without side effects', () => {
    const store = mockStore(state)
    const wrapper = mount(
      <Provider store={store}>
        <ProjectTable />
      </Provider>
    )
    wrapper.find('[data-test="openEditModal"]').first().simulate('click')
    setupProjectData(wrapper, 'edit')
    expect(wrapper.find('[data-test="modal"]')).toHaveLength(2)
    wrapper.find('[data-test="modalClose"]').first().simulate('click')
    expect(wrapper.find('[data-test="modal"]')).toHaveLength(0)
  })

  it('should render Projects', () => {
    const wrapper = shallow(<Projects />)
    expect(wrapper).toMatchSnapshot()
  })
})
