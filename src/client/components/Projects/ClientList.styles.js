import styled from 'styled-components'

export const List = styled.div`
  position: absolute;
  right: 0;
  width: 300px;
`

export const Header = styled.div`
  position: relative;
  height: 55px;
  border-left: 1px solid #eceded;
`

export const Icon = styled.i.attrs({
  className: 'fa fa-times',
})`
  position: absolute;
  top: 20px;
  right: 20px;
  font-size: 18px;
  color: #666;
  cursor: pointer;
`

export const Items = styled.ul`
  background-color: #f5f5f5;
  box-shadow: 5px 5px 15px rgba(0,0,0,.25);
`

export const Item = styled.li`
  display: flex;
  align-items: center;
  height: 55px;
  padding-left: 15px;
  color: #222222;
  overflow: hidden;
  cursor: pointer;

  :hover {
    background-color: #ededed;
  }
`
