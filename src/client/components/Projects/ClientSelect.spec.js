import React from 'react'
import renderer from 'react-test-renderer'
import 'jest-styled-components'
import { Select, Icon } from './ClientSelect.styles'
import ClientSelect from './ClientSelect'

describe('Project/index.styles', () => {
  it('should render Select styles', () => {
    const tree = renderer.create(<Select />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render Icon styles', () => {
    const tree = renderer.create(<Icon />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render ClientSelect styles', () => {
    const tree = renderer.create(<ClientSelect />).toJSON()
    expect(tree).toMatchSnapshot()
  })
})
