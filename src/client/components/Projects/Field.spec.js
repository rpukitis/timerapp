import React from 'react'
import renderer from 'react-test-renderer'
import { shallow } from 'enzyme'
import 'jest-styled-components'
import { Wrapper, Input } from './Field.styles'
import Field from './Field'

describe('Projects/Field.styles', () => {
  it('should render Wrapper styles', () => {
    const tree = renderer.create(<Wrapper />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render Input styles', () => {
    const tree = renderer.create(<Input />).toJSON()
    expect(tree).toMatchSnapshot()
  })
})

describe('Projects/Field component', () => {
  it('should render Field', () => {
    const tree = renderer.create(
      <Field
        value=""
        onChange={() => undefined}
      />
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should handle Field change', () => {
    const handle = jest.fn()
    const wrapper = shallow(
      <Field value="" onChange={handle} />
    )
    wrapper.find('[data-test="input"]')
      .simulate('change', { target: { value: 'awesome' } })
    expect(handle).toHaveBeenCalled()
  })
})
