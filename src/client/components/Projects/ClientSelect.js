import React from 'react'
import { Select, Icon } from './ClientSelect.styles'

const ClientSelect = () => (
  <Select href="#" >
    <Icon />
    SELECT CLIENT
  </Select>
)

export default ClientSelect
