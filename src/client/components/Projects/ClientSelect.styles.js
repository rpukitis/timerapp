import styled from 'styled-components'

export const Select = styled.a`
  position: absolute;
  top: 22px;
  right: 30px;
  font-size: 12px;
  font-weight: 700;
  text-decoration: none;
  text-transform: uppercase;
  color: #595959;

  :hover {
    color: '#4bc800';
  }
`

export const Icon = styled.i.attrs({
  className: 'fa fa-plus',
})`
  margin-right: 5px;
  color: #4bc800;
`
