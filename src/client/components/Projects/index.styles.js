import styled from 'styled-components'

export const Wrapper = styled.div`
  padding: 25px 55px;
`

export const Title = styled.h1`
  font-size: 32px;
  font-weight: 400;
`

export const CreateProject = styled.div`
  display: flex;
  margin-top: 25px;
`

export const CreateButton = styled.button`
  width: 195px;
  height: 55px;
  font-size: 18px;
  font-weight: 600;
  color: #fff;
  background-color: #4BC800;
  box-shadow: inset 0 -2px 0 #45b900;
  cursor: pointer;

  :hover {
    background-color: #43b400;
  }
`

export const EditControls = styled.div`
  display: flex;
  justify-content: space-between;
`

export const EditButtons = styled.div`
  display: flex;
  justify-content: space-between;
  width: 410px;
`

export const EditDelete = CreateButton.extend`
  background-color: #F44336;
  box-shadow: inset 0 -2px 0 #E53935;

  :hover {
    background-color: #D32F2F;
  }
`

export const Table = styled.table`
  width: 100%;
  margin-top: 45px;
  border-collapse: collapse;
  border-spacing: 0;
`

export const HeaderRow = styled.tr`
  border-bottom: 3px solid #e6e6e6;
`

export const HeaderColumn = styled.th`
  height: 55px;
  text-align: left;
  vertical-align: middle;
  font-weight: 600;
  font-size: 15px;
  color: #929292;
`

export const ColumnSpacer = styled.th`
  width: 30px;
  height: 55px;
`

export const Column35 = HeaderColumn.extend`
  width: 35%;
`

export const Column25 = HeaderColumn.extend`
  width: 25%;
`

export const Row = styled.tr`
  border-bottom: 1px solid #e6e6e6;
  cursor: pointer;

  :hover {
    background-color: #eeeeee;
  }
`

export const Column = styled.td`
  height: 55px;
  text-align: left;
  vertical-align: middle;
  font-size: 14px;
  color: #222;
`

export const ProjectColor = styled.span`
  display: inline-block;
  width: 7px;
  height: 7px;
  border-radius: 50%;
  margin: 0 0 1px 5px;
`
