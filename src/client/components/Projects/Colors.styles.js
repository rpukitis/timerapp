import styled, { css } from 'styled-components'
import { checkIcon } from '../../assets/icons'

export const Picker = styled.div`
  position: relative;
  width: 90px;
  height: 55px;
  margin: 0 10px;
  background-color: #fff;
  box-shadow: 0 1px 3px rgba(128,128,128,.2);

  ${props => props.small && css`
    width: 75px;
    margin: 0
  `}
`
export const PreviewLayout = styled.div`
  display: flex;
  align-items: center;
  width: 100%;
  height: 100%;
  cursor: pointer;
`

export const Preview = styled.div`
  width: 25px;
  height: 25px;
  margin: 15px;
  border-radius: 3px;
`

export const Select = styled.i.attrs({
  className: 'fa fa-caret-down',
})`
  color: #929292;
`

export const Choice = styled.div`
  position: absolute;
  top: 65px;
  right: 0px;
  padding: 15px;
  background-color: #fff;
  box-shadow: 0 1px 3px rgba(128,128,128,.2);

  ${props => props.left && css`
    left: 0;
    right: auto;
  `}
`

export const Palette = styled.div`
  display: flex;
  flex-wrap: wrap;
  width: 150px;
`

export const PaletteItem = styled.div`
  width: 24px;
  height: 24px;
  margin: 3px;
  border-radius: 3px;
  cursor: pointer;
`

export const PaletteItemIcon = styled.div`
  width: 24px;
  height: 24px;
  background: url(${checkIcon}) no-repeat 50%;
`
