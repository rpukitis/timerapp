import React from 'react'
import renderer from 'react-test-renderer'
import { mount } from 'enzyme'
import 'jest-styled-components'
import state from '../../store/state'
import { List, Header, Icon, Items, Item } from './ClientList.styles'
import ClientList from './ClientList'

describe('Project/ClientList.styles', () => {
  it('should render List styles', () => {
    const tree = renderer.create(<List />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render Header styles', () => {
    const tree = renderer.create(<Header />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render Icon styles', () => {
    const tree = renderer.create(<Icon />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render Items styles', () => {
    const tree = renderer.create(<Items />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render Item styles', () => {
    const tree = renderer.create(<Item />).toJSON()
    expect(tree).toMatchSnapshot()
  })
})

describe('Project/ClientList component', () => {
  it('should render List', () => {
    const tree = renderer.create(
      <List
        clients={Object.values(state.clients)}
        handleClientSelect={() => undefined}
      />
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should handle select without client', () => {
    const handle = jest.fn()
    const wrapper = mount(
      <ClientList
        clients={Object.values(state.clients)}
        handleWithoutClientSelect={handle}
        handleClientSelect={() => undefined}
      />
    )
    wrapper.find('[data-test="withoutClientItem"]').last().simulate('click')
    expect(handle).toBeCalled()
  })

  it('should handle select client', () => {
    const handle = jest.fn()
    const wrapper = mount(
      <ClientList
        clients={Object.values(state.clients)}
        handleClientSelect={() => handle}
      />
    )
    wrapper.find('[data-test="clientItem"]').last().simulate('click')
    expect(handle).toBeCalled()
  })
})
