import React from 'react'
import Downshift from 'downshift'
import {
  Picker,
  PreviewLayout,
  Preview,
  Select,
  Choice,
  Palette,
  PaletteItem,
  PaletteItemIcon,
} from './Colors.styles'

const Color = ({ left, color, colors, handleColorSelect }) => (
  <Downshift>
    {({ isOpen, toggleMenu }) => (
      <div>
        <Picker small={left} data-test="colorSelect" onClick={toggleMenu}>
          {isOpen &&
            <Choice left={left}>
              <Palette>
                {colors.map((item, index) =>
                  <PaletteItem
                    key={String(index).padStart(3, '0')}
                    style={{ backgroundColor: item }}
                    data-test="colorItem"
                    onClick={handleColorSelect(item)}
                  >
                    {color === item &&
                      <PaletteItemIcon />
                    }
                  </PaletteItem>
                )}
              </Palette>
            </Choice>
          }
          <PreviewLayout>
            <Preview style={{ backgroundColor: color }} />
            <Select />
          </PreviewLayout>
        </Picker>
      </div>
    )}
  </Downshift>
)

export default Color
