import React from 'react'
import renderer from 'react-test-renderer'
import 'jest-styled-components'
import { Select } from './ClientSelected.styles'
import ClientSelected from './ClientSelected'

describe('Project/index.styles', () => {
  it('should render Select styles', () => {
    const tree = renderer.create(<Select />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render ClientSelected styles', () => {
    const tree = renderer.create(<ClientSelected />).toJSON()
    expect(tree).toMatchSnapshot()
  })
})
