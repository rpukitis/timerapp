import React from 'react'
import { branch, renderComponent } from 'recompose'
import Downshift from 'downshift'
import ClientList from './ClientList'
import ClientSelect from './ClientSelect'
import ClientSelected from './ClientSelected'

const OpenClientList = branch(
  ({ client }) => client,
  renderComponent(ClientSelected),
)(ClientSelect)

const Client = (props) => (
  <Downshift>
    {({ isOpen, toggleMenu }) => (
      <div data-test="clientSelect" onClick={toggleMenu}>
        {isOpen ? (
          <ClientList {...props} />
        ) : (
          <OpenClientList {...props} />
        )}
      </div>
    )}
  </Downshift>
)
export default Client
