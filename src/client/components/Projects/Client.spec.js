import React from 'react'
import renderer from 'react-test-renderer'
import { mount } from 'enzyme'
import 'jest-styled-components'
import state from '../../store/state'
import Client from './Client'

describe('Project/Client component', () => {
  it('should render closed Client', () => {
    const tree = renderer.create(<Client />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render open Client', () => {
    const wrapper = mount(
      <Client
        clients={Object.values(state.clients)}
        handleClientSelect={() => undefined}
      />
    )
    wrapper.find('[data-test="clientSelect"]').simulate('click')
    expect(wrapper.html()).toMatchSnapshot()
  })
})
