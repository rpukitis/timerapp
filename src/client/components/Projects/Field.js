import React from 'react'
import { Wrapper, Input } from './Field.styles'

const Field = ({ value, onChange, children }) => (
  <Wrapper>
    {children}
    <Input
      type="text"
      placeholder="Project name..."
      value={value}
      data-test="input"
      onChange={onChange}
    />
  </Wrapper>
)

export default Field
