import React from 'react'
import { Select } from './ClientSelected.styles'

const ClientSelected = ({ client }) => (
  <Select href="#" >
    {client}
  </Select>
)

export default ClientSelected
