import styled from 'styled-components'

export const Select = styled.a`
  position: absolute;
  top: 16px;
  right: 30px;
  padding: 5px 10px;
  font-size: 12px;
  font-weight: 700;
  text-decoration: none;
  text-shadow: 0 1px 0 rgba(70,70,70,.4);
  color: #cc70eb;
  box-shadow: 0 1px 1px rgba(128,128,128,.3);
`
