import React from 'react'
import renderer from 'react-test-renderer'
import { mount } from 'enzyme'
import 'jest-styled-components'
import { COLORS } from './index'
import {
  Picker,
  PreviewLayout,
  Preview,
  Select,
  Choice,
  Palette,
  PaletteItem,
  PaletteItemIcon,
} from './Colors.styles'
import Color from './Color'

describe('Project/Color.styles', () => {
  it('should render Picker styles', () => {
    const tree = renderer.create(<Picker />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render PreviewLayout styles', () => {
    const tree = renderer.create(<PreviewLayout />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render Preview styles', () => {
    const tree = renderer.create(<Preview />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render Select styles', () => {
    const tree = renderer.create(<Select />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render Choice styles', () => {
    const tree = renderer.create(<Choice />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render Palette styles', () => {
    const tree = renderer.create(<Palette />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render PaletteItem styles', () => {
    const tree = renderer.create(<PaletteItem />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render PaletteItemIcon styles', () => {
    const tree = renderer.create(<PaletteItemIcon />).toJSON()
    expect(tree).toMatchSnapshot()
  })
})

describe('Project/Color component', () => {
  it('should render closed Color', () => {
    const tree = renderer.create(
      <Color
        color={COLORS[0]}
        colors={COLORS}
        handleColorSelect={() => undefined}
      />
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render open Color', () => {
    const wrapper = mount(
      <Color
        color={COLORS[0]}
        colors={COLORS}
        handleColorSelect={() => undefined}
      />
    )
    wrapper.find('[data-test="colorSelect"]').last().simulate('click')
    expect(wrapper.html()).toMatchSnapshot()
  })
})
