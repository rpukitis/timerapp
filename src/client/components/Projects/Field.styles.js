import styled from 'styled-components'

export const Wrapper = styled.div`
  position: relative;
  width: 100%;
  height: 55px;
  background-color: #fff;
  box-shadow: 0 1px 3px rgba(128,128,128,.2)
`
export const Input = styled.input`
  width: 100%;
  height: 100%;
  padding-left: 30px;
  border: 0;
  box-shadow: none;
`
