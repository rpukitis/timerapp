import React from 'react'
import { compose, shouldUpdate, withState, withHandlers } from 'recompose'
import Modal from './Modal'
import {
  Background,
  Wrapper,
  Header,
  Title,
  Body,
  AddEntry,
  EntryInput,
  AddEntryButton,
  Entries,
  Entry,
  IcoPencil,
  IcoTimes,
  EditEntryInput,
} from './Taxonomy.styles'

export const Add = ({ type, value, handleChange, handleKeyDown, handleAdd }) => (
  <AddEntry>
    <EntryInput
      type="text"
      value={value}
      placeholder={`New ${type.toLowerCase()} name...`}
      data-test="entryInput"
      onChange={handleChange}
      onKeyDown={handleKeyDown}
    />
    <AddEntryButton
      data-test="entryAddButton"
      onClick={handleAdd}
    >
      Add
    </AddEntryButton>
  </AddEntry>
)

const EntryAdd = compose(
  shouldUpdate(() => false),
  withState('value', 'setValue', ''),
  withHandlers({
    handleChange: ({ setValue }) => (e) => {
      setValue(e.target.value)
    },
    handleKeyDown: ({ value, setValue, onAdd }) => (e) => {
      if (value && e.keyCode === 13) {
        onAdd(value)
        setValue('')
      }
    },
    handleAdd: ({ value, setValue, onAdd }) => (e) => {
      if (!value) {
        return
      }
      onAdd(value)
      setValue('')
    },
  }),
)(Add)

export const List = ({ entries, handleEdit, handleRemove }) => (
  <Entries>
    {entries.map(entry =>
      <Entry key={entry.id}>
        {entry.name}
        <IcoPencil data-test="entryEdit" onClick={handleEdit(entry)} />
        <IcoTimes data-test="entryDelete" onClick={handleRemove(entry)} />
      </Entry>
    )}
  </Entries>
)

const EntryList = compose(
  withHandlers({
    handleEdit: ({ setEntry }) => (entry) => (e) => {
      setEntry(entry)
    },
    handleRemove: ({ onRemove }) => (entry) => (e) => {
      onRemove(entry.id)
    },
  }),
)(List)

export const Edit = ({ type, value, handleChange, handleKeyDown, handleEdit }) => [
  <EditEntryInput
    key="edit-input"
    type="text"
    value={value}
    placeholder={`Edit ${type.toLowerCase()} name...`}
    data-test="editEntryInput"
    onChange={handleChange}
    onKeyDown={handleKeyDown}
  />,
  <AddEntryButton
    key="edit-button"
    data-test="addEntryButton"
    onClick={handleEdit}
  >
    Save
  </AddEntryButton>,
]

const EntryEdit = compose(
  withState('value', 'setValue', ({ entry }) => entry.name),
  withHandlers({
    handleChange: ({ setValue }) => (e) => {
      setValue(e.target.value)
    },
    handleKeyDown: ({ value, entry, setEntry, onUpdate }) => (e) => {
      if (value && e.keyCode === 13) {
        onUpdate(entry.id, value)
        setEntry(null)
      }
    },
    handleEdit: ({ value, entry, setEntry, onUpdate }) => (e) => {
      if (!value) {
        return
      }
      onUpdate(entry.id, value)
      setEntry(null)
    },
  }),
)(Edit)

const Taxonomy = ({ type, entry, entries, setEntry, onAdd, onRemove, onUpdate }) => (
  <div>
    <Background>
      <Wrapper>
        <Header>
          <Title>{`${type}s`}</Title>
        </Header>
        <Body>
          <EntryAdd type={type} onAdd={onAdd} />
          <EntryList
            entries={entries}
            setEntry={setEntry}
            onRemove={onRemove}
          />
        </Body>
      </Wrapper>
    </Background>
    {entry &&
      <Modal
        small
        title={`Edit ${type}`}
        data-test="taxonomyModal"
        onClose={() => setEntry(null)}
      >
        <EntryEdit
          type={type}
          entry={entry}
          setEntry={setEntry}
          onUpdate={onUpdate}
        />
      </Modal>
    }
  </div>
)

export default compose(withState('entry', 'setEntry', null))(Taxonomy)
