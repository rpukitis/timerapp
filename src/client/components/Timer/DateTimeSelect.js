import React, { Component } from 'react'
import { compose, withProps, withState, withHandlers } from 'recompose'
import { connect } from 'react-redux'
import Downshift from 'downshift'
import { setTimerDateTime, resetTimerDateTime } from '../../store/timer'
import { getTimer } from '../../store/selectors'
import Calendar from './Calendar'
import {
  getElapsedTime,
  getElapsedTimeHM,
  getDurationStamp,
  getTimeStamp,
  validateTimeStamp,
} from '../../services'
import {
  Wrapper,
  Select,
  Timer,
  DropDown,
  DropDownHeader,
  TimeCounter as TimeCounterStyle,
  TimeTitle,
  TimeInput,
} from './DateTimeSelect.styles'

export class TimeCounter extends Component {
  componentDidMount () {
    this.interval = setInterval(::this.forceUpdate, 1000)
  }

  componentWillUnmount () {
    clearInterval(this.interval)
  }

  render () {
    const { startedAt, now = new Date().getTime() } = this.props
    const elapsed = getElapsedTime(startedAt, now)
    const duration = getDurationStamp(elapsed)
    return <Timer data-test="timer"> {duration} </Timer>
  }
}

export const TimeDuration = ({ startedAt, stoppedAt }) => (
  <Timer data-test="timer">
    {getDurationStamp(getElapsedTimeHM(startedAt, stoppedAt))}
  </Timer>
)

export const TimeSelect = ({ title, time, onChange, onBlur }) => (
  <TimeCounterStyle>
    <TimeTitle> {title} </TimeTitle>
    <TimeInput
      type="text"
      value={time}
      onChange={onChange}
      onBlur={onBlur}
      data-test="timeSelect"
    />
  </TimeCounterStyle>
)

export const DateTime = ({
  time,
  date,
  startedAt,
  stoppedAt,
  timerTicking,
  handleStateChange,
  handleTimerClick,
  handleTimeChange,
  handleTimeBlur,
  handleDateChange,
}) => (
  <Downshift onStateChange={handleStateChange}>
    {({ isOpen, toggleMenu }) => (
      <div>
        <Wrapper>
          <Select
            showCursor={!timerTicking}
            onClick={handleTimerClick(isOpen, toggleMenu)}
            data-test="timerToggle"
          >
            {date ? (
              <TimeDuration startedAt={startedAt} stoppedAt={stoppedAt} />
            ) : (
              <TimeCounter startedAt={startedAt} />
            )}
          </Select>
          {isOpen &&
            <DropDown>
              <DropDownHeader>
                <TimeSelect
                  title="Start"
                  time={time.startedAt}
                  onChange={handleTimeChange('started', startedAt)}
                  onBlur={handleTimeBlur}
                />
                <TimeSelect
                  title="End"
                  time={time.stoppedAt}
                  onChange={handleTimeChange('stopped', stoppedAt)}
                  onBlur={handleTimeBlur}
                />
              </DropDownHeader>
              <Calendar date={date} onDateChange={handleDateChange} />
            </DropDown>
          }
        </Wrapper>
      </div>
    )}
  </Downshift>
)

const DateTimeSelect = compose(
  withProps(
    ({ startedAt, stoppedAt }) => ({
      timerTicking: startedAt && !stoppedAt,
    })
  ),
  withState('time', 'setTime', { startedAt: 'hh:mm', stoppedAt: 'hh:mm' }),
  withHandlers({
    handleTimerClick: (props) => (isOpen, toggleMenu) => (e) => {
      const {
        date,
        startedAt,
        stoppedAt,
        timerTicking,
        setTime,
        setTimerDateTime,
      } = props

      if (timerTicking) {
        return
      }
      if (isOpen) {
        toggleMenu(e)
        return
      }

      const now = new Date()
      const time = getTimeStamp(now.getHours(), now.getMinutes())
      const timerStarted = startedAt || time
      const timerStopped = stoppedAt || time
      const timerDate = date ||
        new Date(now.getFullYear(), now.getMonth(), now.getDate()).getTime()
      setTime({ startedAt: timerStarted, stoppedAt: timerStopped })
      setTimerDateTime(timerStarted, timerStopped, timerDate)
      toggleMenu(e)
    },
    handleTimeChange: ({ startedAt, stoppedAt, date, setTime, setTimerDateTime }) =>
      (type, defaultTime) => (e) => {
        const value = validateTimeStamp(e.target.value, defaultTime)
        const time = {
          startedAt,
          stoppedAt,
          [`${type}At`]: value,
        }
        setTime({ ...time, [`${type}At`]: e.target.value })
        setTimerDateTime(time.startedAt, time.stoppedAt, date)
      },
    handleTimeBlur: ({ startedAt, stoppedAt, setTime }) => () => {
      setTime({ startedAt, stoppedAt })
    },
    handleDateChange: ({ startedAt, stoppedAt, setTimerDateTime }) => (date) => {
      setTimerDateTime(startedAt, stoppedAt, date)
    },
    handleStateChange: ({ startedAt, stoppedAt, resetTimerDateTime }) => (state) => {
      if (!state.isOpen && startedAt === stoppedAt) {
        resetTimerDateTime()
      }
    },
  }),
)(DateTime)

const mapStateToProps = (state) => ({
  date: getTimer(state).date,
  startedAt: getTimer(state).startedAt,
  stoppedAt: getTimer(state).stoppedAt,
})

export default connect(mapStateToProps, { setTimerDateTime, resetTimerDateTime })(DateTimeSelect)
