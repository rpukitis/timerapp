import React from 'react'
import renderer from 'react-test-renderer'
import { shallow, mount } from 'enzyme'
import 'jest-styled-components'
import configureMockStore from 'redux-mock-store'
import { Provider } from 'react-redux'
import state from '../../store/state'
import {
  Wrapper,
  Select,
  SelectIcon,
  List as ListStyle,
  FilterWrapper,
  FilterInput,
  Tags,
  Tag,
} from './TagSelect.styles'
import Container, {
  Toggle,
  List,
  TagList,
  TagSelect,
} from './TagSelect'

const mockStore = configureMockStore([])

describe('Timer/TagSelect.styles', () => {
  it('should render Wrapper styles', () => {
    const tree = renderer.create(<Wrapper />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render Select styles', () => {
    const tree = renderer.create(<Select />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render SelectIcon styles', () => {
    const tree = renderer.create(<SelectIcon />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render ListStyle styles', () => {
    const tree = renderer.create(<ListStyle />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render FilterWrapper styles', () => {
    const tree = renderer.create(<FilterWrapper />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render FilterInput styles', () => {
    const tree = renderer.create(<FilterInput />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render Tags styles', () => {
    const tree = renderer.create(<Tags />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render Tag styles', () => {
    const tree = renderer.create(<Tag />).toJSON()
    expect(tree).toMatchSnapshot()
  })
})

describe('Timer/TagSelect component', () => {
  it('should render Toggle', () => {
    const tree = renderer.create(<Toggle />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render focused Toggle', () => {
    const tree = renderer.create(<Toggle focus />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should handle Toggle click', () => {
    const handle = jest.fn()
    const wrapper = shallow(<Toggle onClick={handle} />)
    wrapper.find('[data-test="tagToggle"]').simulate('click')
    expect(handle).toHaveBeenCalled()
  })

  it('should render List', () => {
    const nop = () => undefined
    const tree = renderer.create(
      <List
        filterText=""
        tags={Object.values(state.tags)}
        timerTags={[]}
        handleFilterChange={nop}
        handleTagClick={nop}
      />
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render List with selected tags', () => {
    const nop = () => undefined
    const timerTags = Object
      .values(state.tags)
      .map(t => t.id)
    const wrapper = shallow(
      <List
        filterText=""
        tags={Object.values(state.tags)}
        timerTags={timerTags}
        handleFilterChange={nop}
        handleTagClick={nop}
      />
    )
    expect(wrapper).toMatchSnapshot()
  })

  it('should render tag select List with filter', () => {
    const nop = () => undefined
    const tree = renderer.create(
      <List
        filterText="re"
        tags={Object.values(state.tags)}
        timerTags={[]}
        handleFilterChange={nop}
        handleTagClick={nop}
      />
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should handle tag select List filter change', () => {
    const nop = () => undefined
    const handle = jest.fn()
    const wrapper = shallow(
      <List
        filterText=""
        tags={Object.values(state.tags)}
        timerTags={[]}
        handleFilterChange={handle}
        handleTagClick={nop}
      />
    )
    wrapper.find('[data-test="listInput"]').simulate('change', { target: { value: 'ba' } })
    expect(handle).toHaveBeenCalled()
  })

  it('should handle List tag select', () => {
    const nop = () => undefined
    const handle = jest.fn()
    const wrapper = shallow(
      <List
        filterText=""
        tags={Object.values(state.tags)}
        timerTags={[]}
        handleFilterChange={nop}
        handleTagClick={() => handle}
      />
    )
    wrapper.find('[data-test="selectTag"]').first().simulate('click')
    expect(handle).toHaveBeenCalled()
  })

  it('should handle TagList filter input change', () => {
    const nop = () => undefined
    const wrapper = mount(
      <TagList
        tags={Object.values(state.tags)}
        timerTags={[]}
        handleTagClick={nop}
      />
    )
    wrapper.find('[data-test="listInput"]')
      .last().simulate('change', { target: { value: 're' } })
    expect(wrapper.find('[data-test="listInput"]')
      .last().props().value).toBe('re')
  })

  it('should handle TagSelect/TagList visibility toggle', () => {
    const nop = () => undefined
    const wrapper = mount(
      <TagSelect
        tags={Object.values(state.tags)}
        timerTags={[]}
        updateTimerTags={nop}
      />
    )
    expect(wrapper.find('[data-test="tagList"]')).toHaveLength(0)
    wrapper.find('[data-test="tagToggle"]').last().simulate('click')
    expect(wrapper.find('[data-test="tagList"]').length).toBeGreaterThan(0)
  })

  it('should render ProjectSelect', () => {
    const store = mockStore(state)
    const wrapper = mount(
      <Provider store={store}>
        <Container />
      </Provider>
    )
    wrapper.find('[data-test="tagToggle"]').last().simulate('click')
    expect(wrapper.html()).toMatchSnapshot()
  })
})
