import styled, { css } from 'styled-components'
import { leftIcon, rightIcon } from '../../assets/icons'

export const Month = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  border-bottom: 1px solid #ececec;
`

export const MonthSwitch = styled.div`
  width: 50px;
  height: 54px;
  background-repeat: no-repeat;
  background-position: center center;
  background-size: 32px;
  cursor: pointer;

  :hover {
    background-color: '#f5f5f5';
  }
`

export const MonthPrev = MonthSwitch.extend`
  background: url(${leftIcon}) no-repeat 50%;
`

export const MonthNext = MonthSwitch.extend`
  background: url(${rightIcon}) no-repeat 50%;
`

export const MonthTitle = styled.span`
  font-size: 14px;
  color: #000;
`

export const YearTitle = styled.span`
  margin-left: 4px;
  font-size: 14px;
  color: #a3a3a3;
`

export const DayList = styled.ul`
  display: flex;
  border-bottom: 1px solid #ececec;
`

export const DayItem = styled.li`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 54px;
  height: 24px;
  font-size: 11px;
  color: #cecece;
  list-style-type: none;
`

export const DateList = styled.ul`
  display: flex;
  align-items: center;
  height: 46px;
  border-bottom: 1px solid #ececec;
`

export const DateItemO = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 54px;
  height: 100%;
  font-size: 14px;
  color: #000;
  background: #f5f5f5;
  list-style-type: none;
`

export const DateItem = DateItemO.extend`
  height: 35px;
  background-color: transparent;
  border-radius: 23px;
  cursor: pointer;

  :hover {
    background-color: #f5f5f5;
  }

  ${props => props.selected && css`
    color: #fff;
    background-color: #4bc800;

    :hover {
      background-color: #4bc800;
    }
  `}
`
