import styled, { css } from 'styled-components'
import { playIcon, stopIcon, confirmIcon, closeIcon } from '../../assets/icons'

export const Wrapper = styled.div`
  display: flex;
  position: fixed;
  top: 0;
  left: 100px;
  width: calc(100% - 100px);
  height: 68px;
  font: inherit;
  font-size: 18px;
  background-color: #fff;
  box-shadow: 0 2px 8px 0 rgba(0,0,0,.1);
`

export const Description = styled.div`
  flex: 1 0 auto;
  min-width: 320px;
  height: 100%;
`

export const DescriptionInput = styled.input`
  width: 100%;
  height: 100%;
  padding-left: 30px;
  border: 0;
  box-shadow: none;
`

export const TimerToggle = styled.div`
  width: 70px;
  height: 68px;
  cursor: pointer;
  background: url(${playIcon}) no-repeat 50%;

  ${props => props.active && css`
    background: url(${stopIcon}) no-repeat 50%;
  `}

  ${props => props.confirm && css`
    background: url(${confirmIcon}) no-repeat 50%;
  `}
`

export const TimerReset = styled.div`
   width: 40px;
   border-left: 1px solid #eceded;
   cursor: pointer;
   background: url(${closeIcon}) no-repeat 50%;
`
