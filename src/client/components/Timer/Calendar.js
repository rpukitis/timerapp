import React from 'react'
import { compose, withProps, withState, withHandlers, shouldUpdate } from 'recompose'
import cal from 'calendar'
import { wrap, ijToIndex, getSelectedDate } from '../../services'
import {
  Month,
  MonthPrev,
  MonthNext,
  MonthTitle,
  YearTitle,
  DayList,
  DayItem,
  DateList,
  DateItemO,
  DateItem,
} from './Calendar.styles'

const MONTH = [
  'January', 'February', 'March', 'April', 'May', 'June',
  'July', 'August', 'September', 'October', 'November', 'December',
]

const DAY = [
  'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun',
]

export const Calendar = ({
  calendar,
  calendarLength,
  selectedDate,
  yearMonth: { year, month },
  handlePrevMonth,
  handleNextMonth,
  handleDateChange,
}) => (
  <div>
    <Month>
      <MonthPrev onClick={handlePrevMonth} data-test="prevMonth" />
      <div>
        <MonthTitle> {MONTH[month]} </MonthTitle>
        <YearTitle> {year} </YearTitle>
      </div>
      <MonthNext onClick={handleNextMonth} data-test="nextMonth" />
    </Month>
    <DayList>
      {DAY.map(day =>
        <DayItem key={day} > {day} </DayItem>
      )}
    </DayList>
    {calendar.map((week, i) =>
      <DateList key={month + i} >
        {week.map((date, j) =>
          date ? (
            <DateItem
              key={month + ijToIndex(i, j, calendarLength)}
              onClick={handleDateChange(date)}
              selected={selectedDate === date}
              data-test="dateChange"
            >
              {date}
            </DateItem>
          ) : (
            <DateItemO key={month + ijToIndex(i, j, calendarLength)} />
          ),
        )}
      </DateList>
    )}
  </div>
)

export default compose(
  shouldUpdate((prev, next) => prev.date !== next.date),
  withState('yearMonth', 'setYearMonth', ({ date }) => ({
    year: new Date(date).getFullYear(),
    month: new Date(date).getMonth(),
  })),
  withHandlers({
    handlePrevMonth: ({ yearMonth: { year, month }, setYearMonth }) => (e) => {
      setYearMonth({
        year: month - 1 < 0 ? year - 1 : year,
        month: wrap(month - 1, 0, 11),
      })
    },
    handleNextMonth: ({ yearMonth: { year, month }, setYearMonth }) => (e) => {
      setYearMonth({
        year: month + 1 > 11 ? year + 1 : year,
        month: wrap(month + 1, 0, 11),
      })
    },
    handleDateChange: ({ yearMonth: { year, month }, onDateChange }) => (date) => (e) => {
      const fullDate = new Date(year, month, date).getTime()
      onDateChange(fullDate)
    },
  }),
  withProps(
    ({ yearMonth: { year, month } }) => ({
      calendar: new cal.Calendar(1).monthDays(year, month),
    })
  ),
  withProps(
    ({ date, yearMonth: { year, month }, calendar }) => ({
      selectedDate: getSelectedDate(date, year, month),
      calendarLength: calendar.length * 7,
    })
  ),
)(Calendar)
