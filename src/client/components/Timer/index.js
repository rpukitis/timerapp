import React from 'react'
import { compose, withProps, withHandlers } from 'recompose'
import { connect } from 'react-redux'
import * as actions from '../../store/timer'
import { getTimer } from '../../store/selectors'
import ProjectSelect from './ProjectSelect'
import TagSelect from './TagSelect'
import DateTimeSelect from './DateTimeSelect'
import {
  Wrapper,
  Description as DescriptionStyle,
  DescriptionInput,
  TimerToggle as TimerToggleStyle,
  TimerReset,
} from './index.styles'

export const Description = ({ description, handleDescriptionChange }) => (
  <DescriptionStyle>
    <DescriptionInput
      type="text"
      value={description || ''}
      placeholder="What are you working on?"
      data-test="descriptionInput"
      onChange={handleDescriptionChange}
    />
  </DescriptionStyle>
)

export const TimerDescription = compose(
  connect((state) => ({ description: getTimer(state).description }), {
    updateTimerDescription: actions.updateTimerDescription,
  }),
  withHandlers({
    handleDescriptionChange: ({ updateTimerDescription }) => (e) => {
      updateTimerDescription(e.target.value)
    },
  }),
)(Description)

export const Toggle = ({ toggle, toggleStyle, resetTimer }) => [
  <TimerToggleStyle
    key="toggle"
    active={toggleStyle === 'active'}
    confirm={toggleStyle === 'confirm'}
    data-test="toggle"
    onClick={() => toggle(new Date().getTime())}
  />,
  <TimerReset
    key="reset"
    data-test="toggleReset"
    onClick={resetTimer}
  />,
]

export const TimerToggle = compose(
  connect((state) => ({
    date: getTimer(state).date,
    startedAt: getTimer(state).startedAt,
    stoppedAt: getTimer(state).stoppedAt,
  }), {
    startTimer: actions.startTimer,
    resetTimer: actions.resetTimer,
    addEntryFromTimer: actions.addEntryFromTimer,
    addEntryFromDateTime: actions.addEntryFromDateTime,
  }),
  withProps((props) => {
    const {
      date,
      startedAt,
      stoppedAt,
      startTimer,
      addEntryFromTimer,
      addEntryFromDateTime,
    } = props

    if (date && startedAt !== stoppedAt) {
      return {
        toggle: addEntryFromDateTime,
        toggleStyle: 'confirm',
      }
    }

    if (startedAt && !stoppedAt) {
      return {
        toggle: addEntryFromTimer,
        toggleStyle: 'active',
      }
    }

    return {
      toggle: startTimer,
      toggleStyle: 'normal',
    }
  }),
)(Toggle)

const Timer = () => (
  <Wrapper>
    <TimerDescription />
    <ProjectSelect />
    <TagSelect />
    <DateTimeSelect />
    <TimerToggle />
  </Wrapper>
)
export default Timer
