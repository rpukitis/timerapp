import React from 'react'
import renderer from 'react-test-renderer'
import { shallow, mount } from 'enzyme'
import 'jest-styled-components'
import configureMockStore from 'redux-mock-store'
import { Provider } from 'react-redux'
import state from '../../store/state'
import {
  Wrapper,
  WrapperSelect,
  Select,
  ProjectSelect as ProjectSelectStyle,
  ProjectColor,
  ProjectName,
  ProjectWithoutName,
  ClientName,
  ProjectSelectItem,
  List as ListStyle,
  FilterWrapper,
  FilterInput,
  Projects,
  Client,
} from './ProjectSelect.styles'
import Container, {
  EmptyToggle,
  Toggle,
  ProjectToggle,
  List,
  ProjectList,
  ProjectSelect,
} from './ProjectSelect'

const mockStore = configureMockStore([])

describe('Timer/ProjectSelect.styles', () => {
  it('should render Wrapper styles', () => {
    const tree = renderer.create(<Wrapper />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render WrapperSelect styles', () => {
    const tree = renderer.create(<WrapperSelect />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render Select styles', () => {
    const tree = renderer.create(<Select />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render ProjectSelectStyle styles', () => {
    const tree = renderer.create(<ProjectSelectStyle />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render ProjectColor styles', () => {
    const tree = renderer.create(<ProjectColor />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render ProjectName styles', () => {
    const tree = renderer.create(<ProjectName />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render ProjectWithoutName styles', () => {
    const tree = renderer.create(<ProjectWithoutName />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render ClientName styles', () => {
    const tree = renderer.create(<ClientName />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render ProjectSelectItem styles', () => {
    const tree = renderer.create(<ProjectSelectItem />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render ListStyle styles', () => {
    const tree = renderer.create(<ListStyle />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render FilterWrapper styles', () => {
    const tree = renderer.create(<FilterWrapper />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render FilterInput styles', () => {
    const tree = renderer.create(<FilterInput />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render Projects styles', () => {
    const tree = renderer.create(<Projects />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render Client styles', () => {
    const tree = renderer.create(<Client />).toJSON()
    expect(tree).toMatchSnapshot()
  })
})

describe('should render Timer/ProjectSelect component', () => {
  it('should render EmptyToggle', () => {
    const tree = renderer.create(<EmptyToggle />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should handle EmptyToggle onClick', () => {
    const handle = jest.fn()
    const wrapper = mount(<EmptyToggle toggle={handle} />)
    wrapper.find('[data-test="emptyProjectToggle"]').first().simulate('click')
    expect(handle).toHaveBeenCalled()
  })

  it('should render Toggle', () => {
    const tree = renderer.create(
      <Toggle
        project="Awesome Project 001"
        client="Awesome Client 001"
        color="'#06aaf5'"
        toggle={() => undefined}
      />
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should handle Toggle onClick', () => {
    const handle = jest.fn()
    const wrapper = mount(
      <Toggle
        project="Awesome Project 001"
        client="Awesome Client 001"
        color="'#06aaf5'"
        toggle={handle}
      />
    )
    wrapper.find('[data-test="toggleProject"]').first().simulate('click')
    expect(handle).toHaveBeenCalled()
  })

  it('should branch ProjectToggle and render EmptyToggle', () => {
    const tree = renderer.create(<ProjectToggle />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should branch ProjectToggle and render Toggle', () => {
    const tree = renderer.create(
      <ProjectToggle
        project="Awesome Project 001"
        client="Awesome Client 001"
        color="'#06aaf5'"
        toggle={() => undefined}
      />
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })

  const setUpProjectGroup = () => {
    const groupedWithout = { client: 'No client', projects: [{ id: 1, name: 'foo', client: null }] }
    const groupedFoo = { client: 'foo', projects: [{ id: 1, name: 'foo', client: 1 }] }
    const groupedBar = { client: 'bar', projects: [{ id: 2, name: 'bar', client: 2 }, { id: 3, name: 'abc', client: 1 }] }
    return [groupedFoo, groupedBar, groupedWithout]
  }

  it('should render project select List without filter', () => {
    const group = setUpProjectGroup()
    const tree = renderer.create(
      <List
        projects={group}
        filterText=""
        handleFilterChange={() => undefined}
        handleProjectSelect={() => undefined}
      />
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render project select List with filter', () => {
    const group = setUpProjectGroup()
    const tree = renderer.create(
      <List
        projects={group}
        filterText="ba"
        handleFilterChange={() => undefined}
        handleProjectSelect={() => undefined}
      />
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should handle project select List filter change', () => {
    const group = setUpProjectGroup()
    const handle = jest.fn()
    const wrapper = shallow(
      <List
        projects={group}
        filterText=""
        handleFilterChange={handle}
        handleProjectSelect={() => undefined}
      />
    )
    wrapper.find('[data-test="listInput"]').simulate('change', { target: { value: 'ba' } })
    expect(handle).toHaveBeenCalled()
  })

  it('should handle List project deselect', () => {
    const group = setUpProjectGroup()
    const handle = jest.fn()
    const wrapper = shallow(
      <List
        projects={group}
        filterText=""
        handleFilterChange={() => undefined}
        handleProjectSelect={() => handle}
      />
    )
    wrapper.find('[data-test="selectNoProjectItem"]').simulate('click')
    expect(handle).toHaveBeenCalled()
  })

  it('should handle List project select', () => {
    const group = setUpProjectGroup()
    const handle = jest.fn()
    const wrapper = shallow(
      <List
        projects={group}
        filterText=""
        handleFilterChange={() => undefined}
        handleProjectSelect={() => handle}
      />
    )
    wrapper.find('[data-test="selectProjectItem"]').first().simulate('click')
    expect(handle).toHaveBeenCalled()
  })

  it('should handle ProjectList filter input change', () => {
    const group = setUpProjectGroup()
    const wrapper = mount(
      <ProjectList projects={group} handleProjectSelect={() => undefined} />
    )
    wrapper.find('[data-test="listInput"]')
      .last().simulate('change', { target: { value: 'ba' } })
    expect(wrapper.find('[data-test="listInput"]')
      .last().props().value).toBe('ba')
  })

  it('should handle ProjectSelect/ProjectList visibility toggle', () => {
    const group = setUpProjectGroup()
    const wrapper = mount(
      <ProjectSelect projects={group} updateTimerProject={() => undefined} />
    )
    expect(wrapper.find('[data-test="projectList"]')).toHaveLength(0)
    wrapper.find('[data-test="projectToggle"]').last().simulate('click')
    expect(wrapper.find('[data-test="projectList"]').length).toBeGreaterThan(0)
  })

  it('should render ProjectSelect', () => {
    const store = mockStore(state)
    const wrapper = mount(
      <Provider store={store}>
        <Container />
      </Provider>
    )
    wrapper.find('[data-test="projectToggle"]').last().simulate('click')
    expect(wrapper.html()).toMatchSnapshot()
  })
})
