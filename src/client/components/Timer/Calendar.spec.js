import React from 'react'
import renderer from 'react-test-renderer'
import { shallow } from 'enzyme'
import 'jest-styled-components'
import cal from 'calendar'
import Container, { Calendar } from './Calendar'
import {
  Month,
  MonthPrev,
  MonthNext,
  MonthTitle,
  YearTitle,
  DayList,
  DayItem,
  DateList,
  DateItemO,
  DateItem,
} from './Calendar.styles'

describe('Timer/Calendar.styles', () => {
  it('should render Month styles', () => {
    const tree = renderer.create(<Month />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render MontPrev styles', () => {
    const tree = renderer.create(<MonthPrev />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render MontNext styles', () => {
    const tree = renderer.create(<MonthNext />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render MonthTitle styles', () => {
    const tree = renderer.create(<MonthTitle />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render YearTitle styles', () => {
    const tree = renderer.create(<YearTitle />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render DayList styles', () => {
    const tree = renderer.create(<DayList />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render DayItem styles', () => {
    const tree = renderer.create(<DayItem />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render DateList styles', () => {
    const tree = renderer.create(<DateList />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render DateItemO styles', () => {
    const tree = renderer.create(<DateItemO />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render DateItem styles', () => {
    const tree = renderer.create(<DateItem />).toJSON()
    expect(tree).toMatchSnapshot()
  })
})

describe('Timer/Calendar component', () => {
  const yearMonth = { year: 2017, month: 11 }
  const calendar = new cal.Calendar(1).monthDays(yearMonth.year, yearMonth.month)
  const calendarLength = calendar.length * 7
  const selectedDate = 17
  const nop = () => () => 'NOP'

  it('should render Calendar dumb component', () => {
    const tree = renderer.create(
      <Calendar
        calendar={calendar}
        calendarLength={calendarLength}
        yearMonth={yearMonth}
        selectedDate={selectedDate}
        handlePrevMonth={nop}
        handleNextMonth={nop}
        handleDateChange={nop}
      />
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should handle prev month click', () => {
    const handle = jest.fn()
    const wrapper = shallow(
      <Calendar
        calendar={calendar}
        calendarLength={calendarLength}
        yearMonth={yearMonth}
        selectedDate={selectedDate}
        handlePrevMonth={handle}
        handleNextMonth={nop}
        handleDateChange={nop}
      />
    )
    wrapper.find('[data-test="prevMonth"]').simulate('click')
    expect(handle).toHaveBeenCalled()
  })

  it('should handle next month click', () => {
    const handle = jest.fn()
    const wrapper = shallow(
      <Calendar
        calendar={calendar}
        calendarLength={calendarLength}
        yearMonth={yearMonth}
        selectedDate={selectedDate}
        handlePrevMonth={nop}
        handleNextMonth={handle}
        handleDateChange={nop}
      />
    )
    wrapper.find('[data-test="nextMonth"]').simulate('click')
    expect(handle).toHaveBeenCalled()
  })

  it('should handle date change click', () => {
    const handle = jest.fn()
    const wrapper = shallow(
      <Calendar
        calendar={calendar}
        calendarLength={calendarLength}
        yearMonth={yearMonth}
        selectedDate={selectedDate}
        handlePrevMonth={nop}
        handleNextMonth={nop}
        handleDateChange={() => handle}
      />
    )
    wrapper.find('[data-test="dateChange"]').forEach((node) => {
      node.simulate('click')
    })
    expect(handle).toHaveBeenCalledTimes(31)
  })

  it('should render Calendar container component', () => {
    const tree = renderer.create(
      <Container date={'Sun Dec 17 2017 12:00:00 GMT+0200 (EET)'} onDateChange={nop} />
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })
})
