import styled, { css } from 'styled-components'

export const Wrapper = styled.div`
  position: relative;
  width: 90px;
  height: 68px;
`
export const Select = styled.div`
  display: flex;
  height: 100%;
  justify-content: center;
  align-items: center;

  ${props => props.showCursor && css`
      cursor: pointer;
  `}
`

export const Timer = styled.div`
  width: 105px;
  text-align: center;
  fontSize: 22px;
  color: #a3a3a3;
`

export const DropDown = styled.div`
  position: relative;
  left: 50%;
  transform: translateX(-68%);
  width: 364px;
  background: #fff;
  border-top: 1px solid #ececec;
  border-radius: 0 0 3px 3px;
  box-shadow: 0 2px 8px 0 rgba(0,0,0,.1);
`

export const DropDownHeader = styled.div`
  display: flex;
  width: 100%;
  padding-top: 15px;
`

export const TimeCounter = styled.div`
  width: 50%;
`

export const TimeTitle = styled.div`
  padding-left: 15px;
  font-size: 11px;
  color: #a3a3a3;
`

export const TimeInput = styled.input`
  width: 100%;
  padding: 10px 15px;
  font-size: 15px;
  color: #222;
  border-bottom: 1px solid #ececec;
  box-shadow: none;

  :focus {
    border-bottom: 1px solid #000;
  };
`
