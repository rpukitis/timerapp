import React from 'react'
import renderer from 'react-test-renderer'
import { shallow, mount } from 'enzyme'
import 'jest-styled-components'
import configureMockStore from 'redux-mock-store'
import { Provider } from 'react-redux'
import {
  Wrapper,
  Select,
  Timer,
  DropDown,
  DropDownHeader,
  TimeCounter as TimeCounterStyle,
  TimeTitle,
  TimeInput,
} from './DateTimeSelect.styles'
import Container, { TimeCounter, TimeDuration, TimeSelect, DateTime } from './DateTimeSelect'

const mockStore = configureMockStore([])

describe('Timer/DataTimeSelect.styles', () => {
  it('should render Wrapper styles', () => {
    const tree = renderer.create(<Wrapper />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render Select styles', () => {
    const tree = renderer.create(<Select />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render Select styles with show cursor', () => {
    const tree = renderer.create(<Select showCursor />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render Timer styles', () => {
    const tree = renderer.create(<Timer />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render DropDown styles', () => {
    const tree = renderer.create(<DropDown />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render DropDownHeader styles', () => {
    const tree = renderer.create(<DropDownHeader />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render TimeCounterStyle styles', () => {
    const tree = renderer.create(<TimeCounterStyle />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render TimeTitle styles', () => {
    const tree = renderer.create(<TimeTitle />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render TimeInput styles', () => {
    const tree = renderer.create(<TimeInput />).toJSON()
    expect(tree).toMatchSnapshot()
  })
})

describe('should render Timer/DateTimeSelect component', () => {
  it('should render TimerCounter', () => {
    const tree = renderer.create(
      <TimeCounter startedAt={0} now={0} />
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should update TimerCounter on timeout tick', () => {
    jest.useFakeTimers()
    const getTime = Date.prototype.getTime
    Date.prototype.getTime = () => 1000 // eslint-disable-line
    const wrapper = mount(
      <TimeCounter startedAt={1000} />
    )
    Date.prototype.getTime = //eslint-disable-line
      () => 1000 * 60 + 1000 * 60 * 59 + 1000 * 60 * 60 * 12
    jest.runOnlyPendingTimers()
    expect(wrapper.update().text().trim()).toEqual('12:59:59')
    Date.prototype.getTime = getTime // eslint-disable-line
    jest.clearAllTimers()
  })

  it('should render TimeDuration', () => {
    const tree = renderer.create(
      <TimeDuration startedAt={'12:00:00'} stoppedAt={'12:45:00'} />
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render TimeSelect', () => {
    const nop = () => 'NOP'
    const tree = renderer.create(
      <TimeSelect title="Start" time="12:00" onChange={nop} onBlur={nop} />
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should handle TimeSelect time change', () => {
    const handle = jest.fn()
    const nop = () => 'NOP'
    const wrapper = shallow(
      <TimeSelect title="Start" time="12:00" onChange={handle} onBlur={nop} />
    )
    wrapper.find('[data-test="timeSelect"]').simulate('change', { target: { value: '12:45' } })
    expect(handle).toHaveBeenCalled()
  })

  it('should handle TimeSelect time blur', () => {
    const handle = jest.fn()
    const nop = () => 'NOP'
    const wrapper = shallow(
      <TimeSelect title="Start" time="12:00" onChange={nop} onBlur={handle} />
    )
    wrapper.find('[data-test="timeSelect"]').simulate('blur', { target: { value: '12:45' } })
    expect(handle).toHaveBeenCalled()
  })

  it('should render closed DateTime with TimeDuration', () => {
    const nop = () => undefined
    const tree = renderer.create(
      <DateTime
        date="Sun Dec 17 2017 12:00:00 GMT+0200 (EET)"
        startedAt="12:00:00"
        stoppedAt="12:45:00"
        handleTimerClick={nop}
      />
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render closed DateTime with TimeCounter', () => {
    const getTime = Date.prototype.getTime
    Date.prototype.getTime = () => 2000 // eslint-disable-line
    const nop = () => undefined
    const tree = renderer.create(
      <DateTime startedAt={1000} handleTimerClick={nop} />
    ).toJSON()
    expect(tree).toMatchSnapshot()
    Date.prototype.getTime = getTime // eslint-disable-line
  })

  it('should render open DateTime', () => {
    const time = { startedAt: '12:00:00', stoppedAt: '12:45:00' }
    const handleTimerClick = (isOpen, toggleMenu) => (e) => toggleMenu()
    const nop = () => undefined
    const wrapper = mount(
      <DateTime
        time={time}
        date="Sun Dec 17 2017 12:00:00 GMT+0200 (EET)"
        startedAt={time.startedAt}
        stoppedAt={time.stoppedAt}
        handleStateChange={nop}
        handleTimerClick={handleTimerClick}
        handleTimeChange={() => nop}
        handleTimeBlur={nop}
      />
    )
    wrapper.find('[data-test="timerToggle"]').first().simulate('click')
    expect(wrapper.html()).toMatchSnapshot()
  })

  it('should render DateTimeSelect', () => {
    const store = mockStore({ timer: {} })
    const wrapper = mount(
      <Provider store={store}>
        <Container />
      </Provider>
    )
    expect(wrapper.html()).toMatchSnapshot()
  })
})
