import React from 'react'
import { compose, branch, renderComponent, withState, withHandlers } from 'recompose'
import { connect } from 'react-redux'
import Downshift from 'downshift'
import { updateTimerProject } from '../../store/timer'
import { getTimerProject, getProjectsGroupedByClient, getClients } from '../../store/selectors'
import { filterClientGroupByKey } from '../../services'
import {
  Wrapper,
  WrapperSelect,
  Select,
  ProjectSelect as ProjectSelectStyle,
  ProjectColor,
  ProjectName,
  ProjectWithoutName,
  ClientName,
  ProjectSelectItem,
  List as ListStyle,
  FilterWrapper,
  FilterInput,
  Projects,
  Client,
} from './ProjectSelect.styles'

export const EmptyToggle = ({ toggle }) => (
  <WrapperSelect data-test="emptyProjectToggle" onClick={toggle}>
    <Select>
      Project/task
    </Select>
  </WrapperSelect>
)

export const Toggle = ({ toggle, project, client, color }) => (
  <ProjectSelectStyle data-test="toggleProject" onClick={toggle}>
    <ProjectColor style={{ backgroundColor: color }} />
    <div style={{ color: color }}> {project} </div>
    {client &&
      <ClientName> {client} </ClientName>
    }
  </ProjectSelectStyle>
)

export const ProjectToggle = branch(
  ({ project }) => project,
  renderComponent(Toggle),
)(EmptyToggle)

export const List = ({ projects, filterText, handleFilterChange, handleProjectSelect }) => (
  <ListStyle>
    <FilterWrapper>
      <FilterInput
        type="text"
        placeholder="Filter projects"
        value={filterText}
        data-test="listInput"
        onChange={handleFilterChange}
      />
    </FilterWrapper>
    <Projects>
      <Client> No Client </Client>
      <ProjectSelectItem
        data-test="selectNoProjectItem"
        onClick={handleProjectSelect(null)}
      >
        <ProjectColor style={{ backgroundColor: '#cecece' }} />
        <ProjectWithoutName> No Project </ProjectWithoutName>
      </ProjectSelectItem>
      {filterClientGroupByKey(filterText, projects).map(item =>
        <div key={item.client}>
          <Client> {item.client} </Client>
          {item.projects.map(project =>
            <ProjectSelectItem
              key={project.id}
              data-test="selectProjectItem"
              onClick={handleProjectSelect(project.id)}
            >
              <ProjectColor style={{ backgroundColor: project.color }} />
              <ProjectName> {project.name} </ProjectName>
            </ProjectSelectItem>
          )}
        </div>
      )}
    </Projects>
  </ListStyle>
)

export const ProjectList = compose(
  withState('filterText', 'onChange', ''),
  withHandlers({
    handleFilterChange: ({ onChange }) => (e) => onChange(e.target.value),
    handleProjectSelect: ({ updateProject }) => (id) => (e) => updateProject(id),
  }),
)(List)

export const ProjectSelect = ({ projects, project = {}, client = {}, updateTimerProject }) => (
  <Downshift>
    {({ isOpen, toggleMenu }) => (
      <div>
        <Wrapper>
          <ProjectToggle
            project={project.name}
            client={client.name}
            color={project.color}
            data-test="projectToggle"
            toggle={toggleMenu}
          />
          {isOpen &&
            <ProjectList
              projects={projects}
              data-test="projectList"
              updateProject={updateTimerProject}
            />
          }
        </Wrapper>
      </div>
    )}
  </Downshift>
)

const mapStateToProps = (state) => {
  const project = getTimerProject(state)
  return ({
    project,
    projects: getProjectsGroupedByClient(state),
    client: project && getClients(state)[project.client],
  })
}

export default connect(mapStateToProps, { updateTimerProject })(ProjectSelect)
