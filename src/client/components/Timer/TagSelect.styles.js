import styled, { css } from 'styled-components'
import { tagIcon, tagIconHover, searchIcon } from '../../assets/icons'

export const Wrapper = styled.div`
  position: relative;
  width: 46px;
  height: 68px;
`

export const Select = styled.div`
   display: flex;
   height: 100%;
   justify-content: center;
   align-items: center;
   cursor: pointer;
`

export const SelectIcon = styled.div`
  width: 36px;
  height: 36px;
  background: url(${tagIcon}) no-repeat center center;

  ${Select}:hover & {
    background: url(${tagIconHover}) no-repeat center center;
  }

  ${props => props.focus && css`
    background-color: #eaf9e1;
    border-radius: 100px;
  `}

  ${props => props.selected && css`
    background: url(${tagIconHover}) no-repeat center center;
  `}
`

export const List = styled.div`
  position: relative;
  left: 50%;
  transform: translateX(-50%);
  width: 340px;
  background: #fff;
  border-top: 1px solid #ececec;
  border-radius: 0 0 3px 3px;
  box-shadow: 0 2px 8px 0 rgba(0,0,0,.1);
`

export const FilterWrapper = styled.div`
  padding: 15px 15px 15px 15px;
`

export const FilterInput = styled.input`
  width: 100%;
  height: 29px;
  padding-left: 32px;
  font-size: 14px;
  background: #fff url(${searchIcon}) 10px center no-repeat;
  border: 1px solid #ececec;
  box-shadow: none;
`

export const Tags = styled.div`
  display: flex;
  flex-wrap: wrap;
  padding: 15px 0 0 0;
`
export const Tag = styled.div`
  max-width: 300px;
  margin: 5px 3px;
  padding: 5px 10px;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
  font-size: 14px;
  color:  #a3a3a3;
  border: 1px solid #ececec;
  border-radius: 20px;
  background-color: transparent;
  cursor: pointer;

  :hover {
    background-color: #f2f2f2;
    border-color: #f2f2f2;
  },

  ${props => props.selected && css`
    color: #51c623;
    background-color: #E9F8E1;
    border-color: #e9f8e1;

    :hover {
      background-color: #e2f1db;
      border-color: #e2f1db;
    },
  `}
`
