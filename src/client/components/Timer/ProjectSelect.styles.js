import styled from 'styled-components'
import { selectIcon, searchIcon } from '../../assets/icons'

export const Wrapper = styled.div`
  flex: 0 1 auto;
  position: relative;
  min-width: 90px;
  height: 68px;
`

export const WrapperSelect = styled.div`
   display: flex;
   height: 100%;
   justify-content: flex-end;
   align-items: center;
   cursor: pointer;
`

export const Select = styled.div`
  padding-left: 25px;
  font-size: 15px;
  color: #a3a3a3;
  background: url(${selectIcon}) no-repeat 0 center;

  ${WrapperSelect}:hover & {
    color: #4bc800;
  }
`

export const ProjectSelect = styled.div`
  display: flex;
  height: 100%;
  align-items: center;
  margin: 0 10px 0 15px;
  font-size: 15px;
  cursor: pointer;
`
export const ProjectColor = styled.div`
   display: inline-block;
   width: 12px;
   height: 12px;
   border-radius: 50%;
   margin-right: 9px;
`

export const ProjectName = styled.div`
  color: #224;
`

export const ProjectWithoutName = styled.div`
  color: #a3a3a3;
`

export const ClientName = styled.div`
  color: #a3a3a3;

  ::before {
    display: inline-block;
    content: '\\2022';
    margin: 0 7px;
    font-family: Arial;
    font-size: 16px;
    font-weight: 700;
    color: #cecece;
    vertical-align: middle;
  }
`

export const ProjectSelectItem = ProjectSelect.extend`
  width: calc(100% - 120px);
  height: 30px;
  margin: 0 15px 0 6px;
  padding-left: 9px;
  overflow: hidden;
  text-overflow: ellipsis;
  border-radius: 20px;
  cursor: pointer;

  :hover {
    background-color: rgba(0,0,0,.05);
  }
`

export const List = styled.div`
  position: absolute;
  left: 50%;
  width: 340px;
  transform: translateX(-50%);
  padding-bottom: 15px;
  background: #fff;
  border-top: 1px solid #ececec;
  border-radius: 0 0 3px 3px;
  box-shadow: 0 2px 8px 0 rgba(0,0,0,.1);
`

export const FilterWrapper = styled.div`
  padding: 15px 15px 0 15px;
`

export const FilterInput = styled.input`
  width: 100%;
  height: 29px;
  padding-left: 32px;
  font-size: 14px;
  background: #fff url(${searchIcon}) 10px center no-repeat;
  border: 1px solid #ececec;
  box-shadow: none;
`

export const Projects = styled.div`
  display: flex;
  flex-direction: column;
`

export const Client = styled.h2`
  margin-top: 20px;
  margin-bottom: 2px;
  padding-left: 15px;
  font-size: 12px;
  font-weight: 700;
  color: #a3a3a3;
`
