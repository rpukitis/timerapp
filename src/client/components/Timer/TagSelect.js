import React from 'react'
import { compose, withState, withHandlers } from 'recompose'
import { connect } from 'react-redux'
import Downshift from 'downshift'
import { updateTimerTags } from '../../store/timer'
import { getTimer, getTagArray } from '../../store/selectors'
import { filterByKey, toggleElement } from '../../services'
import {
  Wrapper,
  Select,
  SelectIcon,
  List as ListStyle,
  FilterWrapper,
  FilterInput,
  Tags,
  Tag,
} from './TagSelect.styles'

export const Toggle = ({ focus, selected, onClick }) => (
  <Select data-test="tagToggle" onClick={onClick}>
    <SelectIcon focus={focus} selected={selected} />
  </Select>
)

export const List = ({ filterText, handleFilterChange, tags, timerTags, handleTagClick }) => (
  <ListStyle>
    <FilterWrapper>
      <FilterInput
        type="text"
        placeholder="Filter tags"
        value={filterText}
        data-test="listInput"
        onChange={handleFilterChange}
      />
      <Tags>
        {filterByKey(filterText, tags).map(tag =>
          <Tag
            key={tag.id}
            selected={!!timerTags.find(i => i === tag.id)}
            data-test="selectTag"
            onClick={handleTagClick(tag.id)}
          >
            {tag.name}
          </Tag>
        )}
      </Tags>
    </FilterWrapper>
  </ListStyle>
)

export const TagList = compose(
  withState('filterText', 'onFilterTextChange', ''),
  withHandlers({
    handleFilterChange: ({ onChange, onFilterTextChange }) => (e) =>
      onFilterTextChange(e.target.value),
    handleTagClick: ({ timerTags, updateTimerTags }) => (id) => (e) =>
      updateTimerTags(toggleElement(id, timerTags)),
  }),
)(List)

export const TagSelect = ({ tags, timerTags, updateTimerTags }) => (
  <Downshift>
    {({ isOpen, toggleMenu }) => (
      <div>
        <Wrapper>
          <Toggle
            focus={isOpen}
            selected={timerTags && timerTags.length}
            data-test="tagToggle"
            onClick={toggleMenu}
          />
          {isOpen &&
            <TagList
              tags={tags}
              timerTags={timerTags}
              data-test="tagList"
              updateTimerTags={updateTimerTags}
            />
          }
        </Wrapper>
      </div>
    )}
  </Downshift>
)

const mapStateToProps = (state) => ({
  tags: getTagArray(state),
  timerTags: getTimer(state).tags || [],
})

export default connect(mapStateToProps, { updateTimerTags })(TagSelect)
