import React from 'react'
import renderer from 'react-test-renderer'
import { shallow, mount } from 'enzyme'
import 'jest-styled-components'
import configureMockStore from 'redux-mock-store'
import { Provider } from 'react-redux'
import thunk from 'redux-thunk'
import state from '../../store/state'
import { addEntryNextID } from '../../store/entries'
import {
  Wrapper,
  Description as DescriptionStyle,
  DescriptionInput,
  TimerToggle as TimerToggleStyle,
  TimerReset,
} from './index.styles'
import Timer, {
  Description,
  TimerDescription,
  Toggle,
  TimerToggle,
} from './index.js'

const mockStore = configureMockStore([thunk])

describe('Timer/index.styles', () => {
  it('should render Wrapper styles', () => {
    const tree = renderer.create(<Wrapper />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render DescriptionStyle styles', () => {
    const tree = renderer.create(<DescriptionStyle />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render DescriptionInput styles', () => {
    const tree = renderer.create(<DescriptionInput />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render TimerToggleStyle styles', () => {
    const tree = renderer.create(<TimerToggleStyle />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should render TimerReset styles', () => {
    const tree = renderer.create(<TimerReset />).toJSON()
    expect(tree).toMatchSnapshot()
  })
})

describe('Timer/index component', () => {
  it('should render Description', () => {
    const tree = renderer.create(
      <Description description="" handleDescriptionChange={() => undefined} />
    ).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('should handle Description change', () => {
    const handle = jest.fn()
    const wrapper = shallow(
      <Description description="" handleDescriptionChange={handle} />
    )
    wrapper.find('[data-test="descriptionInput"]')
      .simulate('change', { target: { value: 'awesome' } })
    expect(handle).toHaveBeenCalled()
  })

  it('should store description', () => {
    const store = mockStore(state)
    const wrapper = mount(
      <Provider store={store}>
        <TimerDescription />
      </Provider>
    )
    wrapper.find('[data-test="descriptionInput"]').last()
      .simulate('change', { target: { value: 'awesome' } })
    expect(store.getActions()).toEqual([
      { type: 'UPDATE_TIMER_DESCRIPTION', description: 'awesome' },
    ])
  })

  it('should render Toogle', () => {
    const getTime = Date.prototype.getTime
    Date.prototype.getTime = () => 1000 // eslint-disable-line
    const tree = renderer.create(
      <Toggle />
    ).toJSON()
    expect(tree).toMatchSnapshot()
    Date.prototype.getTime = getTime // eslint-disable-line
  })

  it('should render active Toogle', () => {
    const getTime = Date.prototype.getTime
    Date.prototype.getTime = () => 1000 // eslint-disable-line
    const tree = renderer.create(
      <Toggle active />
    ).toJSON()
    expect(tree).toMatchSnapshot()
    Date.prototype.getTime = getTime // eslint-disable-line
  })

  it('should render confirm Toogle', () => {
    const getTime = Date.prototype.getTime
    Date.prototype.getTime = () => 1000 // eslint-disable-line
    const tree = renderer.create(
      <Toggle confirm />
    ).toJSON()
    expect(tree).toMatchSnapshot()
    Date.prototype.getTime = getTime // eslint-disable-line
  })

  it('should handle Toggle click', () => {
    const handle = jest.fn()
    const wrapper = mount(<Toggle toggle={handle} />)
    wrapper.find('[data-test="toggle"]').last().simulate('click')
    expect(handle).toHaveBeenCalled()
  })

  it('should handle reset Toggle click', () => {
    const handle = jest.fn()
    const wrapper = mount(<Toggle resetTimer={handle} />)
    wrapper.find('[data-test="toggleReset"]').last().simulate('click')
    expect(handle).toHaveBeenCalled()
  })

  it('should start timer', () => {
    const getTime = Date.prototype.getTime
    Date.prototype.getTime = () => 1000 // eslint-disable-line
    const store = mockStore(state)
    const wrapper = mount(
      <Provider store={store}>
        <TimerToggle />
      </Provider>
    )
    wrapper.find('[data-test="toggle"]').last().simulate('click')
    expect(store.getActions()).toEqual([
      { type: 'START_TIMER', now: 1000 },
    ])
    Date.prototype.getTime = getTime // eslint-disable-line
  })

  it('should stop timer', () => {
    const getTime = Date.prototype.getTime
    Date.prototype.getTime = () => 2000 // eslint-disable-line
    const store = mockStore({
      ...state,
      timer: {
        ...state.timer,
        startedAt: 1000,
      },
    })
    const wrapper = mount(
      <Provider store={store}>
        <TimerToggle />
      </Provider>
    )
    wrapper.find('[data-test="toggle"]').first().simulate('click')
    expect(store.getActions()).toEqual([
      { type: 'STOP_TIMER', now: 2000 },
      { type: 'RESET_TIMER' },
      {
        type: 'ADD_ENTRY',
        id: addEntryNextID - 1,
        description: undefined,
        project: undefined,
        tags: undefined,
        duration: NaN,
        date: 1000,
      },
    ])
    Date.prototype.getTime = getTime // eslint-disable-line
  })

  it('should reset ticking timer', () => {
    const store = mockStore({
      ...state,
      timer: {
        ...state.timer,
        startedAt: 1000,
      },
    })
    const wrapper = mount(
      <Provider store={store}>
        <TimerToggle />
      </Provider>
    )
    wrapper.find('[data-test="toggleReset"]').first().simulate('click')
    expect(store.getActions()).toEqual([
      { type: 'RESET_TIMER' },
    ])
  })

  it('should confirm timer', () => {
    const getTime = Date.prototype.getTime
    Date.prototype.getTime = () => 2000 // eslint-disable-line
    const store = mockStore({
      ...state,
      timer: {
        ...state.timer,
        startedAt: '00:00',
        stoppedAt: '00:45',
        date: 'Sun Dec 17 2017 00:00:00 GMT+0200 (EET)',
      },
    })
    const wrapper = mount(
      <Provider store={store}>
        <TimerToggle />
      </Provider>
    )
    wrapper.find('[data-test="toggle"]').first().simulate('click')
    expect(store.getActions()).toEqual([
      { type: 'RESET_TIMER' },
      {
        type: 'ADD_ENTRY',
        id: addEntryNextID - 1,
        description: undefined,
        project: undefined,
        tags: undefined,
        duration: 2700000,
        date: 'Sun Dec 17 2017 00:00:00 GMT+0200 (EET)0',
      },
    ])
    Date.prototype.getTime = getTime // eslint-disable-line
  })

  it('should reset set timer', () => {
    const store = mockStore({
      ...state,
      timer: {
        ...state.timer,
        startedAt: '00:00',
        stoppedAt: '00:45',
        date: 'Sun Dec 17 2017 00:00:00 GMT+0200 (EET)',
      },
    })
    const wrapper = mount(
      <Provider store={store}>
        <TimerToggle />
      </Provider>
    )
    wrapper.find('[data-test="toggleReset"]').first().simulate('click')
    expect(store.getActions()).toEqual([
      { type: 'RESET_TIMER' },
    ])
  })

  it('should render Timer', () => {
    const wrapper = shallow(<Timer />)
    expect(wrapper).toMatchSnapshot()
  })
})
