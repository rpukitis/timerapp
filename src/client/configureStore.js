/* eslint-disable */
import { createStore, applyMiddleware, compose } from 'redux'
import { createLogger } from 'redux-logger'
import thunk from 'redux-thunk'
import app from './store'
import initialState from './store/state'

const configureStore = () => {
  let composeEnhancers = compose
  const middlewares = [thunk]

  if (process.env.NODE_ENV !== 'production') {
    composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
    middlewares.push(createLogger())
  }

  return createStore(
    app,
    initialState,
    composeEnhancers(
      applyMiddleware(...middlewares),
    ),
  )
}

export default configureStore
