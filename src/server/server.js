import express from 'express'
import html from '../index.html'

const app = express()

app.get('/api', (req, res) => {
  res.send({
    message: 'I am a server route and can also be hot reloaded!',
  })
})

app.get('*', (req, res) => {
  res.send(html)
})

export default app
